#pragma once

#include "LAWorldSettings.generated.h"

UCLASS()
class LOOKABOUT_API ALAWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, Category = "Music")
	USoundBase* LevelPauseMusic;

	ALAWorldSettings(const FObjectInitializer& OI);

};
