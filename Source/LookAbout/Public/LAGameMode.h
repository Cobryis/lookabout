#pragma once

#include "GameFramework/GameMode.h"
#include "LAGameMode.generated.h"

/**
 * 
 */
UCLASS()
class LOOKABOUT_API ALAGameMode : public AGameMode
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
	class UPostProcessComponent* PostProcessComponent;

public:
	ALAGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	virtual void StartPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	void UpdatePostProcessPause(bool bIsPaused);
};
