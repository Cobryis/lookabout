#pragma once

#include "GameFramework/Pawn.h"
#include "LACharacter.generated.h"

UCLASS()
class LOOKABOUT_API ALACharacter : public APawn
{
	GENERATED_BODY()

public:

	UPROPERTY(Transient)
	const class ULAChapter* Chapter;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float Health;

	UPROPERTY(EditAnywhere, Config, BlueprintReadOnly, Category = LookAbout)
	float MaxHealth;

	ALACharacter();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	void ModifyHealth(float Amount);

	UFUNCTION()
	virtual void ActionPressed();
	UFUNCTION()
	virtual void ActionReleased();

	UFUNCTION()
	void PausePressed();
	UFUNCTION()
	void QuitPressed();

	UFUNCTION()
	void TryFinishText();

	virtual void Tick(float DeltaSeconds) override;
};