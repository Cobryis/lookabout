
#pragma once

#include "LAGameInstance.h"
#include "LAChapterEvent.generated.h"

DECLARE_DELEGATE(FLAChapterEventTrigger);

enum class LOOKABOUT_API ELACommand
{
	EVENT,
	ACTION,
	OPTION = ACTION,
	SCENE,
	CODE,
	CLICK,
	RADAR,
	HEALTH,
	HEALTHMULTI,
	NONE
};

enum class LOOKABOUT_API ELAChapterEventType
{
	TEXT,
	JUMP,
	SUBTITLE,
	MATINEE,
	SOUND,
	ENDGAME,
	CUSTOM,
	HEALTH,
	CURSOR,
	NONE
};

ELAChapterEventType GetChapterEventTypeFromString(const FString& String);
ELACommand GetCommandFromString(const FString& String);

UCLASS()
class LOOKABOUT_API  ULAChapterEvent : public UObject
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	class ALACustomEvent* CustomEvent;

	UPROPERTY(BlueprintReadWrite, Category = "LookAbout")
	bool bTriggered;

	UPROPERTY()
	TSubclassOf<ALACustomEvent> CustomEventClass;

	FString AssetName;
	FString Detail;

	FLAChapterEventTrigger OnTrigger;

	FTimerHandle TimerHandle;
protected:
	virtual void Init(const FLATableRow& TableRow);

public:
	ELAChapterEventType Type;
	ELACommand Command;

	UPROPERTY()
	float TimeOrAngle;

	class ULAChapter* GetOwningChapter() const { return Cast<ULAChapter>(GetOuter()); }

	static ULAChapterEvent* ConstructFromTableRow(ULAChapter* Parent, const FLATableRow& TableRow, FName RowName);

	void Trigger();
	virtual void SetupTimer(FTimerManager& TimerManager);
	virtual void ClearTimer(FTimerManager& TimerManager);

	virtual bool WasTriggered() const { return bTriggered; }

	virtual float GetActivateTime() const;

	virtual void AutoActivate(float FastForward = 0.0f);

	virtual void BindMatinee(const FLATableRow& TableRow);
	virtual void BindSubtitle(const FLATableRow& TableRow);
	virtual void BindText(const FLATableRow& TableRow);
	virtual void BindJump(const FLATableRow& TableRow);
	virtual void BindSound(const FLATableRow& TableRow);
	virtual void BindHealth(const FLATableRow& TableRow);
	virtual void BindEndGame(const FLATableRow& TableRow);
	virtual void BindCursor(const FLATableRow& TableRow);
	
	virtual class ALAHUD* GetHUD() const;

	virtual void InitCustomEvent();
	virtual void ChapterInit(ELAGameMode GameMode);
};
