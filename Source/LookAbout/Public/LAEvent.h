#pragma once

#include "LookAbout.h"
#include "LAChapterEvent.h"
#include "LAEvent.generated.h"

UCLASS()
class LOOKABOUT_API ULAEvent : public ULAChapterEvent
{
	GENERATED_BODY()

protected:
	virtual void Init(const FLATableRow& TableRow) override;

public:
	virtual void ChapterInit(ELAGameMode GameMode) override;

};

UCLASS()
class LOOKABOUT_API ULAHealthEvent : public ULAChapterEvent
{
	GENERATED_BODY()

public:

	/** Can trigger multiple times, resets every time health goes out of the trigger threshold */
	UPROPERTY()
	bool bMulti;

	UPROPERTY(Transient)
	bool bCanTrigger;

protected:
	virtual void Init(const FLATableRow& TableRow) override;

public:
	virtual void ChapterInit(ELAGameMode GameMode) override;

	virtual void OnCharacterHealthUpdated(class ALACharacter* CHaracter);

};

UCLASS()
class LOOKABOUT_API ULACode : public ULAEvent
{
	GENERATED_BODY()

protected:

	virtual void Init(const FLATableRow& TableRow) override;

public:

	virtual void AutoActivate(float FastForward /* = 0.0f */) override;
	virtual void Update(FKey Key);

private:

	UPROPERTY(Transient)
	int32 CurrentIndex;

};

UCLASS()
class LOOKABOUT_API ULARadar : public ULAEvent
{
	GENERATED_BODY()

public:

	UPROPERTY(Transient)
	bool bInvert;

	UPROPERTY(Transient)
	FName RadarTag;

protected:
	virtual void Init(const FLATableRow& TableRow) override;

public:
	virtual void ChapterInit(ELAGameMode GameMode) override;

	virtual void AutoActivate(float FastForward /* = 0.0f */) override;

};