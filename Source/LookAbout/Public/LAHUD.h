#pragma once

#include "GameFramework/HUD.h"

#include "Widgets/Layout/Anchors.h"

#include "LAHUD.generated.h"

class UImage;
class UUserWidget;

UCLASS()
class LOOKABOUT_API ALAHUD : public AHUD
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	TSubclassOf<UUserWidget> MainMenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	TSubclassOf<UUserWidget> MenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	class UTexture2D* LoadTexture;

	/** Time it takes for level to fade out */
	UPROPERTY(EditDefaultsOnly, Category = LookAbout, BlueprintReadOnly)
	float FadeOutLevelTime;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, BlueprintReadOnly)
	float FadeInLevelTime;

	/** Widget that is added to viewport on level fade out */
	UPROPERTY(EditDefaultsOnly, Category = LookAbout, BlueprintReadOnly)
	TSubclassOf<UUserWidget> FadeOutLevelWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, BlueprintReadOnly)
	TSubclassOf<UUserWidget> FadeInLevelWidgetClass;

	/** Pause music to be used if the level itself does not have a pause music set in world settings */
	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	USoundBase* DefaultPauseMusic;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	float MusicFadeTime;

protected:

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, meta = (AllowPrivateAccess = true))
	TSubclassOf<class ULAActionWidget> ActionWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, meta = (AllowPrivateAccess = true))
	TSubclassOf<UUserWidget> MarkerWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, meta = (AllowPrivateAccess = true))
	TSubclassOf<UUserWidget> GameWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, meta = (AllowPrivateAccess = true))
	TSubclassOf<UUserWidget> TextWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, meta = (AllowPrivateAccess = true))
	float HudFOV;

protected:
	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	UUserWidget* MarkerWidget;

	UPROPERTY(Transient)
	UFunction* WidgetOnNode;
	UPROPERTY(Transient)
	UFunction* WidgetOffNode;
	UPROPERTY(Transient)
	UFunction* Activated;

	UPROPERTY(Transient)
	FAnchors MarkerDefaultAnchors;

	UPROPERTY(Transient)
	FVector2D MarkerDefaultPosition;
	
	UPROPERTY(Transient)
	class ULAActionWidget* ReferenceActionWidget;

	UPROPERTY(Transient)
	class UCanvasPanel* CanvasPanel;

	UPROPERTY(Transient)
	UUserWidget* TextWidget;

	UPROPERTY(Transient)
	class UTextBlock* SubtitleWidget;

	UPROPERTY(Transient)
	UUserWidget* GameWidget;

	UPROPERTY(Transient)
	UUserWidget* MenuWidget;

	UPROPERTY(Transient)
	UUserWidget* HealthWidget;
	
	UPROPERTY(Transient)
	UImage* LeftEdgeWidget;

	UPROPERTY(Transient)
	UImage* RightEdgeWidget;

	UPROPERTY(Transient)
	FLinearColor EdgeWidgetColor;

	UPROPERTY(Transient)
	float SubtitleShadowAlpha;

	UPROPERTY(Transient)
	float BarWidgetAlpha;

	UPROPERTY(Transient)
	float HealthWidgetAlpha;

	UPROPERTY(Transient)
	UImage* BarWidget;

protected:
	UPROPERTY(Transient)
	TArray<class ULAActionWidget*> AvailableActions;

protected:
	FTimerHandle SubtitleTimerHandle;
	FTimerHandle TryActionTimerHandle;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout, BlueprintReadOnly)
	float TryActionResetTime;

	UPROPERTY(EditDefaultsOnly, Category = LookAbout)
	float WidgetFadeRate;

	UPROPERTY(Transient)
	bool bCanTryAction;

	UPROPERTY(Transient, Category = LookAbout, BlueprintReadOnly)
	const class ULAChapter* CurrentChapter;

public:
	UPROPERTY(Transient, Category = LookAbout, BlueprintReadOnly)
	bool bActionUnderMarker;

	UPROPERTY(Transient, Category = LookAbout, BlueprintReadOnly)
	bool bTextVisible;

	UPROPERTY(Transient, Category = LookAbout, BlueprintReadOnly)
	bool bSubtitleVisible;

	virtual void AddNewAction(class ULAAction* Action, float FastForward);

	ALAHUD(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void ToggleMenu();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void TryAction();

	UFUNCTION(BlueprintImplementableEvent, Category = LookAbout)
	void OnTryAction();

	UFUNCTION(BlueprintImplementableEvent, Category = LookAbout)
	void OnChapterStarted();

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void ShowText(const FString& Text);
	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void HideText();

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void ShowSubtitle(const FString& Subtitle, float Time);

	virtual void RemoveAvailableAction(class ULAActionWidget* Action);

	virtual float GetHUDFOV() const { return HudFOV; }
	virtual void ChapterStarted(class ULAChapter* Chapter);

	bool IsMenuVisible() const;
	FORCEINLINE bool IsTextVisible() const { return bTextVisible; }

};