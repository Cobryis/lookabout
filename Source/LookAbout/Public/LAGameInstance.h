
#pragma once

#include "GenericPlatform/ICursor.h"

#include "Engine/GameInstance.h"
#include "LAChapter.h"
#include "MoviePlayer.h"

#include "GameFramework/SaveGame.h"

#include "Brushes/SlateDynamicImageBrush.h"

#include "Engine/DataTable.h"

#include "LAGameInstance.generated.h"

struct FSlateColorBrush;

struct LOOKABOUT_API FLALoadingScreenBrush : public FSlateDynamicImageBrush// , public FGCObject
{
	FLALoadingScreenBrush(UTexture2D* Texture2D, const FVector2D& InImageSize)
		: FSlateDynamicImageBrush(Texture2D, InImageSize, Texture2D ? Texture2D->GetFName() : NAME_None)
	{
		// SetResourceObject(Texture2D);
	}

	// virtual void AddReferencedObjects(FReferenceCollector& Collector)
	// {
	// 	if (ResourceObject)
	// 	{
	// 		Collector.AddReferencedObject(ResourceObject);
	// 	}
	// }
};

class LOOKABOUT_API SLALoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLALoadingScreen) {}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, class UTexture2D* Image);

private:
	TSharedPtr<FSlateColorBrush> BlackBackground;

	/** loading screen image brush */
	TSharedPtr<FSlateDynamicImageBrush> ImageBrush;
};

UCLASS()
class LOOKABOUT_API ULASaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	static FString SaveSlotName;

	UPROPERTY(VisibleAnywhere)
	FString CurrentChapterName;
};

USTRUCT(BlueprintType)
struct LOOKABOUT_API FLATableRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Command;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeOrAngle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Type;

	/** Action - filename
	 *  Event - 
	 *
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString AssetName;

	/** Action - filename
	 *	Event - 
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Detail;

	static const TCHAR* GetTCHARName() { return TEXT("LATableRow"); }
};

UCLASS()
class LOOKABOUT_API ULAGameInstance : public UGameInstance
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	ULAChapter* CurrentChapter;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout, meta = (DisplayName = "First Chapter"))
	ULAChapter* StartChapter;

	UPROPERTY(Transient)
	ULAChapter* LastLoadableChapter;

	UPROPERTY(BlueprintReadOnly, Category = LookAbout)
	TArray<ULAChapter*> Chapters;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout, meta = (DisplayName = "Splash Screen Already Shown"))
	bool bHasLoadedOnce;

	FLoadingScreenAttributes LoadingScreen;

	UPROPERTY(Transient)
	TEnumAsByte<EMouseCursor::Type> LastCursorShape;
	UPROPERTY(Transient)
	FName LastCursorContentPath;
	UPROPERTY(Transient)
	FVector2D LastCursorHotSpot;

public:
	void SetCurrentChapter(class ULAChapter* Chapter) 
	{ 
		if (Chapter->bCanBeLoadedInto)
		{
			LastLoadableChapter = Chapter;
		}
		if (this->CurrentChapter != nullptr)
		{
			this->CurrentChapter->OnEndPlay();
		}
		this->CurrentChapter = Chapter;
		SaveGame();
	}

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void ResetToLastGameHardwareCursor();

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void SetGameHardwareCursor(EMouseCursor::Type CursorShape, FName CursorContentPath, FVector2D HotSpot);

	virtual void TransitionToChapter(ULAChapter* NextChapter);

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void SaveGame();
	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void LoadAndStartGame();
	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void ResetSaveGame();
	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual bool DoesSaveGameExist();
	UFUNCTION(BlueprintCallable, Category = LookAbout, meta = (DisplayName = "Jump"))
	virtual bool StartChapterWithName(const FString& ChapterName);

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void RestartLevel();

	virtual void StartGameInstance() override;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	void ChangeSoundclassVolume(USoundClass* SoundClass, float NewVolume);

	UDataTable* LoadCSV(const FString& Filename);
	class ULAChapter* NewChapterFromCSV(FString Filename);

	class ULAChapter* GetCurrentChapter() const { return CurrentChapter; }

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	ELAGameMode GetCurrentGameMode() const;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void QuitToMainMenu();

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	class ALAHUD* GetHUD() const;

	void ShowLoadingScreen() const;

#if WITH_EDITOR
	virtual FGameInstancePIEResult StartPlayInEditorGameInstance(ULocalPlayer* LocalPlayer, const FGameInstancePIEParameters& Params);
#endif // WITH_EDITOR

};