#pragma once

#include "UserWidget.h"
#include "LAActionWidget.generated.h"

UENUM(BlueprintType)
enum class ELAActionWidgetState : uint8
{
	Available,
	Missed,
	Hit,
	Passed,
	None,
};

enum ELAGameMode;

UCLASS()
class LOOKABOUT_API ULAActionWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float MoveSpeed;

	float MarkerPosMin;
	float MarkerPosMax;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float HalfSize;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float ViewportU;

	UPROPERTY(Transient)
	class ULAAction* AssociatedAction;

	UPROPERTY(Transient)
	class ULAChapter* Chapter;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	ELAActionWidgetState State;
	
	UPROPERTY(Transient)
	class UCanvasPanelSlot* MarkerSlot;

public:
	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	bool bFadeOut;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float TimeLeft;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	bool bInitialized;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	bool bUnderMarker;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float InitialAlpha;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float FadeOutTime;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	float Angle;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	class UCanvasPanelSlot* RecastSlot;

	UPROPERTY(Transient, BlueprintReadOnly, Category = LookAbout)
	class ALAHUD* HUD;

	ULAActionWidget(const FObjectInitializer& ObjectInitializer);

	virtual void Init(ULAActionWidget* ReferenceActionWidget, UUserWidget* MarkerWidget, class ULAAction* Action);

	void Trigger();

	virtual void PerformMove(float InDeltaTime);

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void StartFadeOut(float Time);

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void SetState(ELAActionWidgetState NewState);
public:

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void MissedTick(float InDeltaTime);

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void HitTick(float InDeltatime);

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void PassedTick(float InDeltaTime);

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void AvailableTick(float InDeltaTime);

public:
	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void OnMissed();

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void OnHit();

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void OnPassed();

	UFUNCTION(BlueprintNativeEvent, Category = LookAbout)
	void OnAvailable();

	virtual void UpdatePosition(float PlayerYaw);

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void ComputeData();
};