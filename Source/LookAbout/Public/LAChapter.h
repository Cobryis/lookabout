#pragma once

#include "LAChapter.generated.h"

UENUM(BlueprintType)
enum ELAGameMode
{
	RHYTHM		UMETA(DisplayName="Rhythm Mode"),
	EXPLORE		UMETA(DisplayName="Explore Mode"),
	BLANK		UMETA(DisplayName="Blank Mode"),
	NONE
};

UCLASS()
class LOOKABOUT_API ULAChapter : public UObject
{
	GENERATED_BODY()

	UPROPERTY(Transient)
	bool bIsLoading;

	UPROPERTY()
	TArray<class ULAChapterEvent*> Events;

	UPROPERTY()
	TArray<class ULAHealthEvent*> HealthEvents;

private:
	void AddEvent(class ULAChapterEvent* ChapterEvent);

	virtual void Init(const class UDataTable* ChapterTable);
public:

	UPROPERTY(BlueprintReadOnly, Category = LookAbout)
	FString ChapterName;

	UPROPERTY()
	bool bCanBeLoadedInto;

	UPROPERTY(BlueprintReadOnly, Category = LookAbout)
	FString SceneName;

	UPROPERTY(BlueprintReadOnly, Category = LookAbout)
	float NodeTime;

	UPROPERTY()
	float AngleLock;

	UPROPERTY(Transient)
	float bHasAngleLock;

	UPROPERTY(BlueprintReadOnly, Category = LookAbout)
	TEnumAsByte<ELAGameMode> GameMode;

	ULAChapter(const FObjectInitializer& ObjectInitializer);

	static ULAChapter* ConstructFromTable(UObject* Parent, const class UDataTable* ChapterTable, const FString& ChapterName);

	virtual void Start();

	virtual void RegisterHealthEvent(class ULAHealthEvent* HealthEvent);
	virtual void OnCharacterHealthUpdated(class ALACharacter* CHaracter);

	virtual void OnBeginPlay(class ALACharacter* Character);
	virtual void OnEndPlay();

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	class ULAGameInstance* GetGameInstance() const;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	class ALAHUD* GetHUD();
};