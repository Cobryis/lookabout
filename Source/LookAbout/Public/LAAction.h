#pragma once

#include "LAChapterEvent.h"
#include "LAAction.generated.h"

UCLASS()
class LOOKABOUT_API ULAAction : public ULAChapterEvent
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	FString DisplayText;

	virtual void Init(const FLATableRow& TableRow) override;

public:

	virtual void ChapterInit(ELAGameMode GameMode) override;

	const FString& GetDisplayText() const { return DisplayText; }

	virtual float GetActivateTime() const override;

	virtual void AutoActivate(float FastForward /*= 0.0f*/) override;
};

UCLASS()
class LOOKABOUT_API ULAClick : public ULAChapterEvent
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	uint32 NumClicks;

	UPROPERTY()
	FName Tag;

	virtual void Init(const FLATableRow& TableRow) override;

public:
	virtual void ChapterInit(ELAGameMode GameMode) override;

	UFUNCTION()
	virtual void OnActorClicked(AActor* TouchedActor, FKey ButtonPressed);
};