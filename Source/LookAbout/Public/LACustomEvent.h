#pragma once

#include "LACustomEvent.generated.h"

UCLASS(Blueprintable)
class LOOKABOUT_API ALACustomEvent : public AActor
{
	GENERATED_BODY()

public:

	ALACustomEvent();

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Init"))
	void BP_InitEvent(const FString& AssetName, const FString& Detail);

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Trigger"))
	void BP_TriggerEvent();

	virtual void Init(const FString& AssetName, const FString& Detail);
};