#pragma once

#include "GameFramework/PlayerController.h"

#include "LAPlayerController.generated.h"

class ULAChapter;
class ULACode;
class ULARadar;

UCLASS()
class LOOKABOUT_API ALAPlayerController : public APlayerController
{
	GENERATED_BODY()

	UPROPERTY(Transient)
	UAudioComponent* PauseMusicComponent;

	UPROPERTY(Transient)
	TArray<ULACode*> ActiveCodes;
	
	UPROPERTY(Transient)
	TArray<ULARadar*> RadarEvents;

public:
	
	ALAPlayerController();

	virtual bool InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad) override;
	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate /* = FCanUnpause() */) override;

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void TransitionToChapter(ULAChapter* NewChapter);

	UFUNCTION(BlueprintCallable, Category = LookAbout)
	virtual void QuitToMainMenu();

	virtual void Tick(float DeltaSeconds) override;

	void RegisterRadarEvent(ULARadar* RadarEvent);
	void RegisterCode(ULACode* NewCode);

protected:

	UPROPERTY(Transient)
	ULAChapter* NextChapter;

	UPROPERTY(Transient)
	float TransitionTimeRemaining;

};