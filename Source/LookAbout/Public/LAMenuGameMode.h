#pragma once

#include "GameFramework/GameMode.h"

#include "LAMenuGameMode.generated.h"

UCLASS()
class LOOKABOUT_API ALAMenuGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ALAMenuGameMode();

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> MenuWidgetClass;

	UPROPERTY(Transient)
	UUserWidget* MenuWidget;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

};