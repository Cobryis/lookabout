
#include "LAMenuGameMode.h"
#include "LookAbout.h"

#include "LAPlayerController.h"
#include "LAGameInstance.h"

#include "Blueprint/UserWidget.h"

#include "LAHUD.h"

ALAMenuGameMode::ALAMenuGameMode()
{
	PlayerControllerClass = ALAPlayerController::StaticClass();
	DefaultPawnClass = nullptr;

	static ConstructorHelpers::FClassFinder<ALAHUD> HUDClassFinder(TEXT("/Game/HUD"));
	if (HUDClassFinder.Succeeded())
	{
		HUDClass = HUDClassFinder.Class;
		MenuWidgetClass = GetDefault<ALAHUD>(HUDClassFinder.Class)->MainMenuWidgetClass;
	}
}

void ALAMenuGameMode::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);

	if (MenuWidgetClass != nullptr)
	{
		MenuWidget = CreateWidget<UUserWidget>(GetGameInstance(), MenuWidgetClass);
		MenuWidget->AddToViewport();
	}
	else
	{
		UE_LOG(LALog, Error, TEXT("HUD was not provided MenuWidgetClass."));
	}

	FInputModeGameAndUI InputMode;
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	InputMode.SetHideCursorDuringCapture(false);
	PC->SetInputMode(InputMode);
}

void ALAMenuGameMode::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (GetGameInstance())
	{
		Cast<ULAGameInstance>(GetGameInstance())->bHasLoadedOnce = true;
	}

	if (MenuWidgetClass != nullptr)
	{
		MenuWidget->RemoveFromParent();
	}
}
