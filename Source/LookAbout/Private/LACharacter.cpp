
#include "LACharacter.h"
#include "LookAbout.h"

#include "LAHUD.h"
#include "LAGameMode.h"
#include "LAChapter.h"
#include "LAGameInstance.h"

ALACharacter::ALACharacter()
{
	MaxHealth = 100.f;
	Health = MaxHealth;
}

void ALACharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Chapter = Cast<ULAGameInstance>(GetGameInstance())->GetCurrentChapter();

	if (Chapter == nullptr)
	{
		Chapter = GetDefault<ULAChapter>();
	}

	FInputActionBinding& PerformAction = PlayerInputComponent->BindAction(TEXT("PerformAction"), IE_Pressed, this, &ALACharacter::ActionPressed);
	PerformAction.bExecuteWhenPaused = true;

	FInputActionBinding& FinishText = PlayerInputComponent->BindAction(TEXT("FinishRead"), IE_Released, this, &ALACharacter::TryFinishText);
	FinishText.bExecuteWhenPaused = true;


	FInputActionBinding& Pause = PlayerInputComponent->BindAction(TEXT("Pause"), IE_Pressed, this, &ALACharacter::PausePressed);
	Pause.bExecuteWhenPaused = true;

	PlayerInputComponent->BindAction(TEXT("Quit"), IE_Pressed, this, &ALACharacter::QuitPressed);
	Pause.bExecuteWhenPaused = true;
}

void ALACharacter::ModifyHealth(float Amount)
{
	Health += Amount;
	if (ULAGameInstance* GI = Cast<ULAGameInstance>(GetGameInstance()))
	{
		if (GI->GetCurrentChapter())
		{
			GI->GetCurrentChapter()->OnCharacterHealthUpdated(this);
		}
	}
}

void ALACharacter::PausePressed()
{
	ALAHUD* HUD = Cast<ALAHUD>(static_cast<APlayerController*>(GetController())->GetHUD());
	if (HUD)
	{
		HUD->ToggleMenu();
	}
}

void ALACharacter::QuitPressed()
{
	GEngine->Exec(GetWorld(), TEXT("quit"));
}

void ALACharacter::TryFinishText()
{
	ALAHUD* HUD = Cast<ALAHUD>(static_cast<APlayerController*>(GetController())->GetHUD());
	if (HUD)
	{
		HUD->HideText();
	}
}

void ALACharacter::ActionPressed()
{
	ALAHUD* HUD = Cast<ALAHUD>(static_cast<APlayerController*>(GetController())->GetHUD());
	if (HUD)
	{
		HUD->TryAction();
	}
}

void ALACharacter::ActionReleased()
{

}

void ALACharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
