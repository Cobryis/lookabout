// Fill out your copyright notice in the Description page of Project Settings.

#include "LAGameMode.h"
#include "LookAbout.h"

#include "LAChapter.h"
#include "LACharacter.h"
#include "LAGameInstance.h"
#include "LAHUD.h"

#include "Engine/PostProcessVolume.h"
#include "Engine/Scene.h"

#include "LAPlayerController.h"

ALAGameMode::ALAGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultPawnClass = ALACharacter::StaticClass();

	PlayerControllerClass = ALAPlayerController::StaticClass();

	PostProcessComponent = CreateDefaultSubobject<UPostProcessComponent>(TEXT("PostProcess"));
	PostProcessComponent->bUnbound = true;

	PostProcessComponent->bEnabled = false;

	static ConstructorHelpers::FClassFinder<ALAHUD> HUDClassFinder(TEXT("/Game/HUD"));
	HUDClass = HUDClassFinder.Class;
}

void ALAGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

}

void ALAGameMode::StartPlay()
{
	if (ULAChapter* const Chapter = Cast<ULAGameInstance>(GetGameInstance())->GetCurrentChapter())
	{
		Chapter->OnBeginPlay(Cast<ALACharacter>(UGameplayStatics::GetPlayerPawn(this, 0)));
	}

	Super::StartPlay();

}

void ALAGameMode::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

// 	ULAChapter* const Chapter = Cast<ULAGameInstance>(GetGameInstance())->GetCurrentChapter();
// 	if (Chapter)
// 	{
// 		Chapter->OnEndPlay();
// 	}
}

void ALAGameMode::UpdatePostProcessPause(bool bIsPaused)
{
	// PostProcess->bEnabled = bIsPaused;
}
