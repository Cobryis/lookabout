
#include "LAChapter.h"
#include "LookAbout.h"

#include "LAAction.h"
#include "LAEvent.h"
#include "LAGameInstance.h"
#include "LACharacter.h"
#include "LAAction.h"
#include "LAHUD.h"

#include "Blueprint/UserWidget.h"

ULAChapter::ULAChapter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeTime = 8.0f;
}

ULAChapter* ULAChapter::ConstructFromTable(UObject* Parent, const class UDataTable* ChapterTable, const FString& ChapterName)
{
	ULAChapter* const Chapter = NewObject<ULAChapter>(Parent, *ChapterName);
	Chapter->ChapterName = ChapterName;
	Chapter->Init(ChapterTable);
	return Chapter;
}

void ULAChapter::Init(const UDataTable* ChapterTable)
{
	auto CommandIt = ChapterTable->GetRowMap().CreateConstIterator();

	const FLATableRow* const FirstCommand = (const FLATableRow*)CommandIt.Value();

	SceneName = TEXT("");
	GameMode = ELAGameMode::BLANK;

	if (GetCommandFromString(FirstCommand->Command) == ELACommand::SCENE)
	{
		FString SceneURL = FString::Printf(TEXT("/Game/Scenes/%s"), *FirstCommand->AssetName);
		if (!FirstCommand->AssetName.IsEmpty() && !GEngine->MakeSureMapNameIsValid(SceneURL))
		{
			UE_LOG(LALog, Warning, TEXT("WARNING: The map '%s' did not exist when loading chapter '%s'"), *SceneURL, *ChapterName);
		}
		else
		{
			SceneName = FirstCommand->AssetName.IsEmpty() ? TEXT("") : SceneURL;
		}

		if (!SceneName.IsEmpty())
		{
			bCanBeLoadedInto = true;
		}

		if (FirstCommand->Type.Equals(TEXT("RHYTHM"), ESearchCase::IgnoreCase))
		{
			GameMode = ELAGameMode::RHYTHM;
			NodeTime = FirstCommand->TimeOrAngle;

			if (NodeTime <= 0.0f || NodeTime >= BIG_NUMBER)
			{
				UE_LOG(LALog, Warning, TEXT("Invalid Detail (node speed) provided for %s"), *ChapterTable->GetName());
				NodeTime = 5.0f;
			}
		}
		else if (FirstCommand->Type.Equals(TEXT("EXPLORE"), ESearchCase::IgnoreCase))
		{
			GameMode = ELAGameMode::EXPLORE;

			NodeTime = FirstCommand->TimeOrAngle;

			AngleLock = FMath::Abs(FCString::Atof(*FirstCommand->Detail));

			bHasAngleLock = AngleLock < 180.f && AngleLock > 0.f;

			if (NodeTime <= 0.0f || NodeTime >= BIG_NUMBER)
			{
				UE_LOG(LALog, Warning, TEXT("Invalid Detail (node speed) provided for %s"), *ChapterTable->GetName());
				NodeTime = 10.0f;
			}
		}
		else if (FirstCommand->Type.Equals(TEXT("BLANK"), ESearchCase::IgnoreCase))
		{
			GameMode = ELAGameMode::BLANK;
		}
		else
		{
			UE_LOG(LALog, Warning, TEXT("No GameMode provided in Type for SCENE command in %s"), *ChapterName);
		}

		++CommandIt;
	}

	for (; CommandIt; ++CommandIt)
	{
		const FLATableRow* const CommandRow = (const FLATableRow*)CommandIt.Value();

		ULAChapterEvent* const ChapterEvent = ULAChapterEvent::ConstructFromTableRow(this, *CommandRow, CommandIt.Key());

		if (ChapterEvent)
		{
			if (GameMode != ELAGameMode::BLANK || !ChapterEvent->IsA(ULAAction::StaticClass()))
			{
				AddEvent(ChapterEvent);
			}
			else
			{
				UE_LOG(LALog, Warning, TEXT("Chapter with BLANK game mode has ACTION or OPTION (should only have EVENT)"));
			}
		}

	}
	
}

void ULAChapter::AddEvent(class ULAChapterEvent* ChapterEvent)
{
	Events.AddUnique(ChapterEvent);
}

void ULAChapter::Start()
{
	ULAGameInstance* const GameInstance = GetGameInstance();
	GameInstance->SetCurrentChapter(this);

	UE_LOG(LALog, Log, TEXT("Starting chapter: %s"), *ChapterName);

	if (!SceneName.IsEmpty())
	{
		GameInstance->ShowLoadingScreen();
		UGameplayStatics::OpenLevel(GetOuter(), *SceneName);
		bIsLoading = true;
	}
	else
	{
		OnBeginPlay(Cast<ALACharacter>(GameInstance->GetFirstLocalPlayerController()->GetPawn()));
	}
}

void ULAChapter::RegisterHealthEvent(class ULAHealthEvent* HealthEvent)
{
	HealthEvents.AddUnique(HealthEvent);
}

void ULAChapter::OnCharacterHealthUpdated(class ALACharacter* CHaracter)
{
	for (ULAHealthEvent* HealthEvent : HealthEvents)
	{
		HealthEvent->OnCharacterHealthUpdated(CHaracter);
	}
}

void ULAChapter::OnBeginPlay(ALACharacter* Character)
{
	// initial load will result in bIsLoading being false, therefore we need to start the game
	if (!bIsLoading && !SceneName.IsEmpty())
	{
		FTimerDelegate StartDelegate;
		StartDelegate.BindLambda([this]
		{
			Start();
		});
		FTimerHandle DummyHandle;

		GetOuter()->GetWorld()->GetTimerManager().SetTimer(DummyHandle, StartDelegate, .1f, false);
		return;
	}

	bIsLoading = false;

	UE_LOG(LALog, Log, TEXT("Begin play for chapter: %s"), *ChapterName);

	FTimerManager& TimerManager = GetOuter()->GetWorld()->GetTimerManager();

	ALAHUD* HUD = GetHUD();
	HUD->ChapterStarted(this);

	for (ULAChapterEvent* Event : Events)
	{
		Event->ChapterInit(GameMode);
	}
}

void ULAChapter::OnEndPlay()
{
	FTimerManager& TimerManager = GetOuter()->GetWorld()->GetTimerManager();

	for (ULAChapterEvent* Event : Events)
	{
		Event->ClearTimer(TimerManager);
	}

	HealthEvents.Empty();
}

class ALAHUD* ULAChapter::GetHUD()
{
	return Cast<ALAHUD>(GetGameInstance()->GetHUD());
}

ULAGameInstance* ULAChapter::GetGameInstance() const
{
	return Cast<ULAGameInstance>(GetOuter());
}
