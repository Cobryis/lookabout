
#include "LAActionWidget.h"
#include "LookAbout.h"

#include "UserWidget.h"
#include "UMG.h"
#include "WidgetLayoutLibrary.h"
#include "Components/RichTextBlock.h"

#include "LAChapter.h"
#include "LAAction.h"

#include "LAHUD.h"

float GetViewportSizeNotScaledX(UObject* Context)
{
	return UWidgetLayoutLibrary::GetViewportSize(Context).X / UWidgetLayoutLibrary::GetViewportScale(Context);
}

float GetXInViewportSpace(UCanvasPanelSlot* Slot, float ViewportSizeX)
{
	return	(Slot->GetAnchors().Minimum.X * ViewportSizeX) + Slot->GetPosition().X;
}

float GetUInViewportSpace(UCanvasPanelSlot* Slot, float ViewportSizeX)
{
	return GetXInViewportSpace(Slot, ViewportSizeX) / ViewportSizeX;
}

ULAActionWidget::ULAActionWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bUnderMarker = false;
	bInitialized = false;

	State = ELAActionWidgetState::None;
}

void ULAActionWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bInitialized)
	{
		ComputeData();

		bool bIsGamePaused = UGameplayStatics::IsGamePaused(GetOwningPlayer());

		if (!bIsGamePaused)
		{
			PerformMove(InDeltaTime);
		}

		FLinearColor WidgetColor = ColorAndOpacity;

		if (bFadeOut)
		{
			TimeLeft -= InDeltaTime;
			float Alpha = FadeOutTime <= 0.0f ? 0.0f : InitialAlpha * (TimeLeft / FadeOutTime);
			if (Alpha >= 0.0f)
			{
				WidgetColor.A = Alpha;
			}
			else
			{
				RemoveFromParent();

				HUD->RemoveAvailableAction(this);
			}
		}
		else
		{
			WidgetColor.A = FMath::Lerp(WidgetColor.A, bIsGamePaused ? 0.f : InitialAlpha, InDeltaTime * 3.f);
		}

		SetColorAndOpacity(WidgetColor);
	}
}

void ULAActionWidget::PerformMove(float InDeltaTime)
{
	if (bInitialized)
	{

		switch (State)
		{
		case ELAActionWidgetState::Missed:
			MissedTick(InDeltaTime);
			break;
		case ELAActionWidgetState::Hit:
			HitTick(InDeltaTime);
			break;
		case ELAActionWidgetState::Available:
			AvailableTick(InDeltaTime);
			break;
		case ELAActionWidgetState::Passed:
			PassedTick(InDeltaTime);
			break;
		}

	}
}

void ULAActionWidget::Init(ULAActionWidget* ReferenceActionWidget, UUserWidget* MarkerWidget, ULAAction* Action)
{
	AssociatedAction = Action;

	MarkerSlot = Cast<UCanvasPanelSlot>(MarkerWidget->Slot);

	HUD = Cast<ALAHUD>(GetOwningPlayer()->GetHUD());

	RecastSlot = Cast<UCanvasPanelSlot>(Slot);
	UCanvasPanelSlot* const ReferenceSlot = Cast<UCanvasPanelSlot>(ReferenceActionWidget->Slot);

	RecastSlot->SetSize(ReferenceSlot->GetSize());
	RecastSlot->SetAutoSize(ReferenceSlot->GetAutoSize());
	RecastSlot->SetAnchors(ReferenceSlot->GetAnchors());
	RecastSlot->SetAlignment(ReferenceSlot->GetAlignment());
	RecastSlot->SetOffsets(ReferenceSlot->GetOffsets());
	RecastSlot->SetZOrder(ReferenceSlot->GetZOrder());

	auto FoundWidget = WidgetTree->FindWidget(TEXT("Text"));
	
	if (auto TextBlock = Cast<UTextBlock>(FoundWidget))
	{
		TextBlock->SetText(FText::FromString(Action->GetDisplayText()));
	}
	else if (auto RichTextBlock = Cast<URichTextBlock>(FoundWidget))
	{
		RichTextBlock->SetText(FText::FromString(Action->GetDisplayText()));
	}
	else
	{
		UE_LOG(LALog, Warning, TEXT("ActionWidget is missing TextBlock named 'Text'"));
	}

	Chapter = AssociatedAction->GetOwningChapter();

	if (Chapter->GameMode == ELAGameMode::EXPLORE)
	{
		FAnchors OldAnchor = RecastSlot->GetAnchors();
		FAnchors NewAnchor;
		NewAnchor.Minimum = FVector2D(.5f, OldAnchor.Minimum.Y);
		NewAnchor.Maximum = FVector2D(.5f, OldAnchor.Maximum.Y);
		RecastSlot->SetAnchors(NewAnchor);

		Angle = Action->TimeOrAngle;

	}
	else if (Chapter->GameMode == ELAGameMode::RHYTHM)
	{
		ViewportU = GetUInViewportSpace(RecastSlot, GetViewportSizeNotScaledX(this));
	}

	ForceLayoutPrepass();

	bInitialized = true;

	InitialAlpha = ColorAndOpacity.A;

	SetState(ELAActionWidgetState::Available);

	ComputeData();
}

void ULAActionWidget::ComputeData()
{
	const float ViewportSizeX = GetViewportSizeNotScaledX(this);

	const float MarkerViewportX = GetXInViewportSpace(MarkerSlot, ViewportSizeX);

	const float SelectableRange = (MarkerSlot->GetSize().X * .5f) - 10.f;

	HalfSize = (GetDesiredSize().X * .5f) - 10.f;

	MarkerPosMin = MarkerViewportX - SelectableRange;
	MarkerPosMax = MarkerViewportX + SelectableRange;

	if (Chapter->GameMode == ELAGameMode::RHYTHM)
	{
		const float TimeTillCenteredOnMarker = Chapter->NodeTime;
		MoveSpeed = -((ViewportU * ViewportSizeX) - MarkerViewportX) / TimeTillCenteredOnMarker;
	}

}

void ULAActionWidget::OnHit_Implementation()
{
	if (AssociatedAction)
	{
		if (State == ELAActionWidgetState::Available)
		{
			AssociatedAction->Trigger();
			State = ELAActionWidgetState::Hit;

			StartFadeOut(1.0f);
		}
	}
	else
	{
		UE_LOG(LALog, Error, TEXT("No Action associated with ActionWidget"));
	}
}

void ULAActionWidget::OnMissed_Implementation()
{
	// StartFadeOut(1.0f);
}

void ULAActionWidget::OnPassed_Implementation()
{
	StartFadeOut(1.0f);

	HUD->RemoveAvailableAction(this);
}

void ULAActionWidget::OnAvailable_Implementation()
{

}

void ULAActionWidget::StartFadeOut(float Time)
{
	bFadeOut = true;
	TimeLeft = FadeOutTime = Time;
}

void ULAActionWidget::SetState(ELAActionWidgetState NewState)
{
	if (NewState != State)
	{
		switch (NewState)
		{
		case ELAActionWidgetState::Available:
			OnAvailable();
			break;
		case ELAActionWidgetState::Hit:
			OnHit();
			break;
		case ELAActionWidgetState::Missed:
			OnMissed();
			break;
		case ELAActionWidgetState::Passed:
			OnPassed();
			break;
		}

		State = NewState;
	}
}

void ULAActionWidget::Trigger()
{
	SetState(ELAActionWidgetState::Hit);
}

void ULAActionWidget::MissedTick_Implementation(float InDeltaTime)
{

}

void ULAActionWidget::HitTick_Implementation(float InDeltaTime)
{

}

void ULAActionWidget::PassedTick_Implementation(float InDeltaTime)
{
	const FVector2D& Pos = RecastSlot->GetPosition();

	RecastSlot->SetPosition(FVector2D(Pos.X + (MoveSpeed * InDeltaTime), Pos.Y));
}

void ULAActionWidget::AvailableTick_Implementation(float InDeltaTime)
{
	if (Chapter->GameMode == ELAGameMode::RHYTHM)
	{
		FVector2D Pos = RecastSlot->GetPosition();
		RecastSlot->SetPosition(FVector2D(Pos.X + (MoveSpeed * InDeltaTime), Pos.Y));
	}

	float PosX = GetXInViewportSpace(RecastSlot, GetViewportSizeNotScaledX(this));

	const float LeftPos = PosX - HalfSize;
	const bool bLeftInRange = LeftPos < MarkerPosMax;
	const float RightPos = PosX + HalfSize;
	const bool bRightInRange = RightPos > MarkerPosMin;

	if (bLeftInRange && bRightInRange)
	{
		bUnderMarker = true;

		HUD->bActionUnderMarker = true;
	}
	else
	{
		if ((Chapter->GameMode == ELAGameMode::RHYTHM) && bUnderMarker)
		{
			SetState(ELAActionWidgetState::Passed);
		}
		bUnderMarker = false;
	}

	InvalidateLayoutAndVolatility();
}

void ULAActionWidget::UpdatePosition(float PlayerYaw)
{
	float HalfFOV = HUD->GetHUDFOV() / 2.0f;

	float LeftAngle = PlayerYaw - HalfFOV;
	float RightAngle = PlayerYaw + HalfFOV;

	float MyAngle = Angle;

	if (RightAngle >= 360.f && MyAngle < LeftAngle)
	{
		MyAngle += 360.f;
	}
	else if (LeftAngle < 0.f && MyAngle > RightAngle)
	{
		MyAngle -= 360.f;
	}
	
	if (MyAngle > LeftAngle && MyAngle < RightAngle)
	{
		SetVisibility(ESlateVisibility::Visible);

		const float ViewportSizeX = GetViewportSizeNotScaledX(this);

		const float NewPosX = FMath::GetMappedRangeValueClamped(FVector2D(LeftAngle, RightAngle), FVector2D(0.0f, ViewportSizeX), MyAngle);

		const float FadeSize = HalfSize * 2.0f;

		float RightDist;

		if (NewPosX < FadeSize)
		{
			SetColorAndOpacity(FLinearColor(ColorAndOpacity.R, ColorAndOpacity.B, ColorAndOpacity.G, (NewPosX / FadeSize) * InitialAlpha));
		}
		else if ((RightDist = ViewportSizeX - NewPosX) < FadeSize)
		{
			SetColorAndOpacity(FLinearColor(ColorAndOpacity.R, ColorAndOpacity.B, ColorAndOpacity.G, (RightDist / FadeSize) * InitialAlpha));
		}
		else
		{
			SetColorAndOpacity(FLinearColor(ColorAndOpacity.R, ColorAndOpacity.B, ColorAndOpacity.G, InitialAlpha));
		}

		float PosY = RecastSlot->GetPosition().Y;
		RecastSlot->SetPosition(FVector2D(NewPosX - RecastSlot->GetAnchors().Minimum.X * ViewportSizeX, PosY));
	}
	else
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}
