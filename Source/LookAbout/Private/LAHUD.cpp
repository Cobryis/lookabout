
#include "LAHUD.h"
#include "LookAbout.h"

#include "LAActionWidget.h"
#include "UMG.h"
#include "Blueprint/WidgetTree.h"
#include "WidgetLayoutLibrary.h"
#include "LAAction.h"

#include "Components/RichTextBlock.h"

ALAHUD::ALAHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TryActionResetTime = .5f;
	WidgetFadeRate = 5.f;
	HudFOV = 90.0f;

	FadeOutLevelTime = .5f;
	MusicFadeTime = .5f;

	SetTickableWhenPaused(true);
}

void ALAHUD::BeginPlay()
{
	Super::BeginPlay();

	if (CurrentChapter == nullptr)
	{
		CurrentChapter = GetDefault<ULAChapter>();
	}

	if (FadeInLevelTime > KINDA_SMALL_NUMBER)
	{
		if (FadeInLevelWidgetClass)
		{
			if (Cast<ULAGameInstance>(GetGameInstance())->bHasLoadedOnce)
			{
				UUserWidget* FadeWidget = CreateWidget<UUserWidget>(GetGameInstance(), FadeInLevelWidgetClass);
				FadeWidget->AddToViewport(4);
				FInputModeGameAndUI InputMode;
				InputMode.SetHideCursorDuringCapture(false);
				InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
				GetOwningPlayerController()->SetInputMode(InputMode);
				GetOwningPlayerController()->bShowMouseCursor = true;
			}
		}
	}
}

void ALAHUD::ChapterStarted(ULAChapter* Chapter)
{
	CurrentChapter = Chapter;

	if (GameWidget == nullptr)
	{
		if (GameWidgetClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("GameWidget was not specified on HUD"));
			return;
		}

		if (ActionWidgetClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("ActionWidget was not specified on HUD"));
		}

		if (MarkerWidgetClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("MarkerWidget was not specified on HUD"));
		}

		if (TextWidgetClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("TextWidget was not specified on HUD"));
		}

		GameWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), GameWidgetClass);

		ReferenceActionWidget = nullptr;

		GameWidget->WidgetTree->ForEachWidget([this](UWidget* Widget)
		{
			if (CanvasPanel == nullptr && Widget->IsA(UCanvasPanel::StaticClass()))
			{
				CanvasPanel = Cast<UCanvasPanel>(Widget);
			}
			else if (ReferenceActionWidget == nullptr && ActionWidgetClass && Widget->IsA(ActionWidgetClass))
			{
				ReferenceActionWidget = Cast<ULAActionWidget>(Widget);
			}
			else if (MarkerWidget == nullptr && MarkerWidgetClass && Widget->IsA(MarkerWidgetClass))
			{
				MarkerWidget = Cast<UUserWidget>(Widget);
			}
			else if (TextWidget == nullptr && TextWidgetClass && Widget->IsA(TextWidgetClass))
			{
				TextWidget = Cast<UUserWidget>(Widget);
			}
			else if (SubtitleWidget == nullptr && Widget->GetFName() == TEXT("SubtitleText"))
			{
				SubtitleWidget = Cast<UTextBlock>(Widget);
			}
			else if (LeftEdgeWidget == nullptr && Widget->GetFName() == TEXT("LeftEdge"))
			{
				LeftEdgeWidget = Cast<UImage>(Widget);
			}
			else if (RightEdgeWidget == nullptr && Widget->GetFName() == TEXT("RightEdge"))
			{
				RightEdgeWidget = Cast<UImage>(Widget);
			}
			else if (BarWidget == nullptr && Widget->GetFName() == TEXT("Bar"))
			{
				BarWidget = Cast<UImage>(Widget);
			}
			else if (HealthWidget == nullptr && Widget->GetFName() == TEXT("HealthBar"))
			{
				HealthWidget = Cast<UUserWidget>(Widget);
			}
		});

		if (LeftEdgeWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find LeftEdgeWidget named 'LeftEdge'"));
		}
		else
		{
			EdgeWidgetColor = LeftEdgeWidget->ColorAndOpacity;
			LeftEdgeWidget->ColorAndOpacity = FLinearColor::Transparent;
		}

		if (RightEdgeWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find RightEdgeWidget named 'RightEdge'"));
		}
		else
		{
			RightEdgeWidget->ColorAndOpacity = FLinearColor::Transparent;
		}

		if (SubtitleWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find SubtitleWidget named 'SubtitleText'"));
		}
		else
		{
			FLinearColor Color = SubtitleWidget->ColorAndOpacity.GetSpecifiedColor();
			Color.A = 0.f;
			FLinearColor ShadowColor = SubtitleWidget->ShadowColorAndOpacity;
			ShadowColor.A = 0.f;
			SubtitleWidget->SetColorAndOpacity(Color);
			SubtitleWidget->SetShadowColorAndOpacity(ShadowColor);
		}

		if (TextWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find TextWidget"));
		}
		else
		{
			FLinearColor Color = TextWidget->ColorAndOpacity;
			Color.A = 0.f;
			TextWidget->SetColorAndOpacity(Color);
		}

		if (BarWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find BarWidget named 'Bar'"));
		}
		else
		{
			BarWidgetAlpha = BarWidget->ColorAndOpacity.A;
			BarWidget->ColorAndOpacity.A = 0.f;
		}

		if (HealthWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find HealthWidget named 'HealthBar'"));
		}
		else
		{
			HealthWidgetAlpha = HealthWidget->ColorAndOpacity.A;
			HealthWidget->ColorAndOpacity.A = 0.f;
		}

		if (MarkerWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find MarkerWidget"));
		}
		else
		{
			UCanvasPanelSlot* MarkerSlot = Cast<UCanvasPanelSlot>(MarkerWidget->Slot);
			MarkerDefaultAnchors = MarkerSlot->GetAnchors();
			MarkerDefaultPosition = MarkerSlot->GetPosition();

			WidgetOnNode = MarkerWidget->FindFunction(TEXT("WidgetOnNode"));
			WidgetOffNode = MarkerWidget->FindFunction(TEXT("WidgetOffNode"));
			Activated = MarkerWidget->FindFunction(TEXT("Activated"));
		}

		if (ReferenceActionWidget == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Could not find ActionWidget"));
		}
		else
		{
			ReferenceActionWidget->SetVisibility(ESlateVisibility::Hidden);
		}

		GameWidget->AddToViewport();

		if (MainMenuWidgetClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("MenuWidget was not specified on HUD"));
		}
		else
		{
			MenuWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), MenuWidgetClass);
			MenuWidget->AddToViewport(2);
			MenuWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	bCanTryAction = true;

	if (MarkerWidget)
	{
		UCanvasPanelSlot* MarkerSlot = Cast<UCanvasPanelSlot>(MarkerWidget->Slot);
		if (CurrentChapter->GameMode == ELAGameMode::EXPLORE)
		{
			FAnchors NewAnchors = MarkerDefaultAnchors;
			NewAnchors.Minimum = FVector2D(.5f, NewAnchors.Minimum.Y);
			NewAnchors.Maximum = FVector2D(.5f, NewAnchors.Maximum.Y);
			MarkerSlot->SetAnchors(NewAnchors);
			FVector2D NewPos = MarkerSlot->GetPosition();
			NewPos.X = 0.f;
			MarkerSlot->SetPosition(NewPos);
		}
		else
		{
			MarkerSlot->SetAnchors(MarkerDefaultAnchors);
			MarkerSlot->SetPosition(MarkerDefaultPosition);
		}

		MarkerWidget->ForceLayoutPrepass();
	}

	OnChapterStarted();
}

bool ALAHUD::IsMenuVisible() const
{
	return MenuWidget ? MenuWidget->IsVisible() : false;
}

void ALAHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (GameWidget != nullptr)
	{
		GameWidget->RemoveFromViewport();
	}

	GetWorld()->GetTimerManager().ClearTimer(TryActionTimerHandle);

	GetWorld()->GetTimerManager().ClearTimer(SubtitleTimerHandle);
}

void ALAHUD::Tick(float DeltaTime)
{
	if (CurrentChapter == GetDefault<ULAChapter>())
	{
		return;
	}

	if (WidgetOnNode && WidgetOffNode)
	{
		if (bActionUnderMarker)
		{
			MarkerWidget->ProcessEvent(WidgetOnNode, nullptr);
		}
		else
		{
			MarkerWidget->ProcessEvent(WidgetOffNode, nullptr);
		}
	}
	//MarkerWidget->SetIsEnabled(bActionUnderMarker);

	Super::Tick(DeltaTime);

	for (int i = 0; i < AvailableActions.Num();)
	{
		ULAActionWidget* Action = AvailableActions[i];
		if (!Action->bInitialized)
		{
			AvailableActions.RemoveAt(i);
		}
		else
		{
			++i;
		}
	}

	bool bIsGamePaused = UGameplayStatics::IsGamePaused(this);
	bool bInExploreModeAndNotPaused = !bIsGamePaused && CurrentChapter->GameMode == ELAGameMode::EXPLORE;

	const FLinearColor& SubtitleColor = SubtitleWidget->ColorAndOpacity.GetSpecifiedColor();
	float SubtitleAlpha = FMath::Lerp(SubtitleColor.A, bSubtitleVisible && !bIsGamePaused ? 1.f : 0.f, DeltaTime * WidgetFadeRate);
	FSlateColor NewSubColor = FLinearColor(SubtitleColor.R, SubtitleColor.G, SubtitleColor.B, SubtitleAlpha);
	SubtitleWidget->SetColorAndOpacity(NewSubColor);
	FLinearColor SubtitleShadowColor = SubtitleWidget->ShadowColorAndOpacity;
	SubtitleShadowColor.A = SubtitleShadowAlpha * SubtitleAlpha;
	SubtitleWidget->SetShadowColorAndOpacity(SubtitleShadowColor);

	const FLinearColor& TextColor = TextWidget->ColorAndOpacity;
	float TextAlpha = FMath::Lerp(TextColor.A, bTextVisible ? 1.f : 0.f, DeltaTime * WidgetFadeRate);
	TextWidget->SetColorAndOpacity(FLinearColor(TextColor.R, TextColor.G, TextColor.B, TextAlpha));

	bool bShowLeftEdge = LeftEdgeWidget->IsHovered() && bInExploreModeAndNotPaused;
	FLinearColor LeftEdgeColor = EdgeWidgetColor;
	LeftEdgeColor.A = FMath::Lerp(LeftEdgeWidget->ColorAndOpacity.A, bShowLeftEdge ? EdgeWidgetColor.A : 0.f, DeltaTime * WidgetFadeRate);
	LeftEdgeWidget->SetColorAndOpacity(LeftEdgeColor);

	bool bShowRightEdge = RightEdgeWidget->IsHovered() && bInExploreModeAndNotPaused;
	FLinearColor RightEdgeColor = EdgeWidgetColor;
	RightEdgeColor.A = FMath::Lerp(RightEdgeWidget->ColorAndOpacity.A, bShowRightEdge ? EdgeWidgetColor.A : 0.f, DeltaTime * WidgetFadeRate);
	RightEdgeWidget->SetColorAndOpacity(RightEdgeColor);

	FLinearColor BarColor = BarWidget->ColorAndOpacity;
	BarColor.A = FMath::Lerp(BarColor.A, bIsGamePaused || CurrentChapter->GameMode == ELAGameMode::BLANK ? 0.f : BarWidgetAlpha, DeltaTime * WidgetFadeRate);
	BarWidget->SetColorAndOpacity(BarColor);

	FLinearColor HealthColor = HealthWidget->ColorAndOpacity;
	HealthColor.A = FMath::Lerp(HealthColor.A, bIsGamePaused ? 0.f : HealthWidgetAlpha, DeltaTime * WidgetFadeRate);
	HealthWidget->SetColorAndOpacity(HealthColor);

	bActionUnderMarker = false;

	if (bInExploreModeAndNotPaused && GetOwningPlayerController())
	{

		float MoveHorizontal = 0.0f;

		float ViewYaw = GetOwningPawn()->GetViewRotation().Yaw;

		float WrappedViewYaw = ViewYaw;
		if (ViewYaw > 180.f)
		{
			WrappedViewYaw -= 360.f;
		}

		if (RightEdgeWidget->IsHovered())
		{
			if (!CurrentChapter->bHasAngleLock || WrappedViewYaw < CurrentChapter->AngleLock)
			{
				MoveHorizontal = CurrentChapter->NodeTime;
			}
		}
		else if (LeftEdgeWidget->IsHovered())
		{
			if (!CurrentChapter->bHasAngleLock || WrappedViewYaw > -CurrentChapter->AngleLock)
			{
				MoveHorizontal = -CurrentChapter->NodeTime;
			}
		}

		GetOwningPlayerController()->AddYawInput(MoveHorizontal*DeltaTime);

		for (ULAActionWidget* Action : AvailableActions)
		{
			Action->UpdatePosition(ViewYaw);
		}
	}
}

void ALAHUD::ToggleMenu()
{
	bool bShow = (MenuWidget->GetVisibility() == ESlateVisibility::Hidden);
	MenuWidget->SetVisibility(bShow ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
	FCanUnpause CanUnpauseForMenu;
	CanUnpauseForMenu.BindLambda([this]
	{
		return (MenuWidget->GetVisibility() == ESlateVisibility::Hidden);
	});
	GetOwningPlayerController()->SetPause(bShow, CanUnpauseForMenu);
}

void ALAHUD::ShowText(const FString& Text)
{
	if (CurrentChapter == GetDefault<ULAChapter>())
	{
		return;
	}

	if (TextWidget && TextWidget->IsValidLowLevel())
	{
		bTextVisible = true;

		FCanUnpause UnpauseDelegate;
		UnpauseDelegate.BindLambda([this]
		{
			return !bTextVisible;
		});
		GetOwningPlayerController()->SetPause(true, UnpauseDelegate);

		auto FoundWidget = TextWidget->WidgetTree->FindWidget(TEXT("Text"));
		if (auto TextBlock = Cast<UTextBlock>(FoundWidget))
		{
			TextBlock->SetText(FText::FromString(Text));
		}
		else if (auto RichTextBlock = Cast<URichTextBlock>(FoundWidget))
		{
			RichTextBlock->SetText(FText::FromString(Text));
		}
		else
		{
			UE_LOG(LALog, Error, TEXT("TextWidget is missing Text named Text"));
		}
	}
}

void ALAHUD::HideText()
{
	bool bMenuShown = (MenuWidget->GetVisibility() != ESlateVisibility::Hidden);
	if (bMenuShown)
	{
		return;
	}

	if (bTextVisible)
	{
		bTextVisible = false;

		GetOwningPlayerController()->SetPause(false);
	}
}

static TAutoConsoleVariable<int32> CVarShowSubtitles(TEXT("ShowSubtitles"), 1, TEXT(""));

void ALAHUD::ShowSubtitle(const FString& Subtitle, float Time)
{
	if (CurrentChapter == GetDefault<ULAChapter>() || CVarShowSubtitles.GetValueOnGameThread() == 0)
	{
		return;
	}

	if (IsValid(SubtitleWidget))
	{
//		if (auto Normal = Cast<UTextBlock>(SubtitleWidget))
		{
			SubtitleWidget->SetText(FText::FromString(Subtitle));
		}
//		else if (auto Rich = Cast<URichTextBlock>(SubtitleWidget))
//		{
//			Rich->SetText(FText::FromString(Subtitle));
//		}

		bSubtitleVisible = true;

		FTimerDelegate TimerDelegate;
		TimerDelegate.BindLambda([this]()
		{
			bSubtitleVisible = false;
		});
		GetWorld()->GetTimerManager().SetTimer(SubtitleTimerHandle, TimerDelegate, Time, false);
	}
}

void ALAHUD::TryAction()
{
	if (CurrentChapter == GetDefault<ULAChapter>())
	{
		return;
	}

	if (bTextVisible)
	{
		HideText();
	}
	else
	{
		// don't perform action if game is paused
		if (UGameplayStatics::IsGamePaused(this))
		{
			return;
		}

		if (Activated != nullptr)
		{
			MarkerWidget->ProcessEvent(Activated, nullptr);
		}

		if (bCanTryAction && AvailableActions.Num() > 0)
		{
			ELAGameMode GameMode = Cast<ULAGameInstance>(GetGameInstance())->GetCurrentChapter()->GameMode;

			auto IsActionUnderMarker = [](ULAActionWidget* ActionWidget) { return ActionWidget ? ActionWidget->bUnderMarker : false; };

			ULAActionWidget* NextAction = nullptr;

			if (GameMode == ELAGameMode::EXPLORE)
			{
				for (int i = 0; i < AvailableActions.Num(); ++i)
				{
					if (IsActionUnderMarker(AvailableActions[i]))
					{
						NextAction = AvailableActions[i];
						break;
					}
				}
			}
			else if (GameMode == ELAGameMode::RHYTHM)
			{
				NextAction = AvailableActions[0];

				if (!IsActionUnderMarker(NextAction))
				{
					NextAction = nullptr;
				}
				else
				{
					AvailableActions.RemoveAt(0);
				}

			}

			if (NextAction)
			{
				NextAction->Trigger();
			}
		}

		bCanTryAction = false;

		FTimerDelegate TimerDelegate;
		TimerDelegate.BindLambda([this]
		{
			bCanTryAction = true;
		});
		GetWorld()->GetTimerManager().SetTimer(TryActionTimerHandle, TimerDelegate, TryActionResetTime, false);
	}

}

void ALAHUD::AddNewAction(class ULAAction* Action, float FastForward)
{
	if (Action)
	{
		if (ReferenceActionWidget && ReferenceActionWidget->IsValidLowLevel())
		{
			ULAActionWidget* ActionWidget = CreateWidget<ULAActionWidget>(GetOwningPlayerController(), ActionWidgetClass);
			CanvasPanel->AddChildToCanvas(ActionWidget);
			ActionWidget->Init(ReferenceActionWidget, MarkerWidget, Action);

			if (Action->GetOwningChapter()->GameMode)
			{
				if (FMath::Abs(FastForward) >= SMALL_NUMBER)
				{
					ActionWidget->PerformMove(FastForward);

					UE_LOG(LALog, Warning, TEXT("Action was FastForwarded: %s"), *Action->GetFullName());
				}
			}

			AvailableActions.Add(ActionWidget);
		}
		else
		{
			UE_LOG(LALog, Error, TEXT("ActionWidget missing from GameWidget"));
		}
	}
	else
	{
		UE_LOG(LALog, Error, TEXT("No Action was provided for LAHUD::AddNewRhythmAction"));
	}
}

void ALAHUD::RemoveAvailableAction(ULAActionWidget* Action)
{
	AvailableActions.RemoveSingle(Action);
}
