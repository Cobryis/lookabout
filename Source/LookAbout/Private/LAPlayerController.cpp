
#include "LAPlayerController.h"
#include "LookAbout.h"

#include "LAChapter.h"
#include "LAEvent.h"
#include "LAGameInstance.h"
#include "LAGameMode.h"
#include "LAHUD.h"
#include "LAWorldSettings.h"

#include "AudioThread.h"
#include "AudioDevice.h"
#include "ActiveSound.h"
#include "Blueprint/UserWidget.h"

ALAPlayerController::ALAPlayerController()
{
	PrimaryActorTick.bTickEvenWhenPaused = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
}

bool ALAPlayerController::InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	bool bResult = Super::InputKey(Key, EventType, AmountDepressed, bGamepad);

	bool bIsUsedByPlayer = bResult;
	if (!bIsUsedByPlayer)
	{
		bIsUsedByPlayer = PlayerInput->ActionMappings.ContainsByPredicate([&Key](const FInputActionKeyMapping& ActionMapping)
		{
			return ActionMapping.Key == Key;
		});
	}

	if (!bIsUsedByPlayer)
	{
		bIsUsedByPlayer = PlayerInput->AxisMappings.ContainsByPredicate([&Key](const FInputAxisKeyMapping& ActionMapping)
		{
			return ActionMapping.Key == Key;
		});
	}

	ALAHUD* HUD = Cast<ALAHUD>(GetHUD());

	if (!bIsUsedByPlayer && (!IsPaused() || (HUD->IsTextVisible() && !HUD->IsMenuVisible())))
	{
		bool bValid = Key.IsValid() && (Key.ToString().Len() == 1) && !(Key.IsMouseButton() && Key.IsGamepadKey() && Key.IsModifierKey() && Key.IsFloatAxis() && Key.IsVectorAxis());

		if (bValid)
		{
			for (ULACode* Code : ActiveCodes)
			{
				Code->Update(Key);
			}
		}
	}

	return bResult;
}

bool ALAPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate)
{
	bool bWasAlreadyPaused = UGameplayStatics::IsGamePaused(this);

	bool bResult = Super::SetPause(bPause, CanUnpauseDelegate);

	if (UGameplayStatics::IsGamePaused(this))
	{
		if (!bWasAlreadyPaused)
		{
			if (ALAHUD* HUD = Cast<ALAHUD>(GetHUD()))
			{
				USoundBase* PauseMusic = HUD->DefaultPauseMusic;
				if (ALAWorldSettings* WorldSettings = Cast<ALAWorldSettings>(GetWorld()->GetWorldSettings()))
				{
					if (WorldSettings->LevelPauseMusic)
					{
						PauseMusic = WorldSettings->LevelPauseMusic;
					}
				}

				if (PauseMusic)
				{
					PauseMusicComponent = UGameplayStatics::SpawnSound2D(this, PauseMusic);
					PauseMusicComponent->FadeIn(HUD->MusicFadeTime);
				}
			}
		}
	}
	else if (PauseMusicComponent)
	{
		if (ALAHUD* HUD = Cast<ALAHUD>(GetHUD()))
		{
			PauseMusicComponent->FadeOut(HUD->MusicFadeTime, 0.f);
		}
		else
		{
			PauseMusicComponent->FadeOut(.2f, 0.f);
		}

		PauseMusicComponent = nullptr;
	}
	

	if (ALAGameMode* GameMode = GetWorld()->GetAuthGameMode<ALAGameMode>())
	{
		GameMode->UpdatePostProcessPause(IsPaused());
	}

	if (ULAGameInstance* GI = Cast<ULAGameInstance>(GetGameInstance()))
	{
		if (IsPaused())
		{
			GetWorld()->GetGameViewport()->SetHardwareCursor(EMouseCursor::Default, NAME_None, FVector2D(0.f, 0.f));
		}
		else
		{
			GI->ResetToLastGameHardwareCursor();
		}
	}

	return bResult;
}

void ALAPlayerController::TransitionToChapter(ULAChapter* NewChapter)
{
	if (TransitionTimeRemaining > 0.f)
	{
		return;
	}

	NextChapter = NewChapter;

	ULAGameInstance* GI = static_cast<ULAGameInstance*>(GetGameInstance());
	if (GI->GetCurrentChapter())
	{
		GI->GetCurrentChapter()->OnEndPlay();
	}

	float FadeOutTime = 0.f;

	if (NextChapter == nullptr || !NextChapter->SceneName.IsEmpty()) // don't play fade if there's no new scene
	{
		if (ALAHUD* HUD = Cast<ALAHUD>(GetHUD()))
		{
			FadeOutTime = HUD->FadeOutLevelTime;
			if (FadeOutTime > KINDA_SMALL_NUMBER)
			{
				if (HUD->FadeOutLevelWidgetClass)
				{
					UUserWidget* FadeWidget = CreateWidget<UUserWidget>(this, HUD->FadeOutLevelWidgetClass);
					FadeWidget->AddToViewport(4);
					FInputModeGameOnly InputMode;
					InputMode.SetConsumeCaptureMouseDown(false);
					SetInputMode(InputMode);
					bShowMouseCursor = false;
				}
			}
		}
	}

	if (FadeOutTime > KINDA_SMALL_NUMBER)
	{
		TransitionTimeRemaining = FadeOutTime + .05f;

		FAudioDevice* AudioDevice = GetWorld()->GetAudioDevice();
		FAudioThread::RunCommandOnAudioThread([AudioDevice, FadeOutTime]()
		{
			for (FActiveSound* ActiveSound : AudioDevice->GetActiveSounds())
			{
				if (ActiveSound)
				{
					ActiveSound->TargetAdjustVolumeMultiplier = 0.f;
					ActiveSound->TargetAdjustVolumeStopTime = ActiveSound->PlaybackTime + FadeOutTime;
					ActiveSound->bFadingOut = true;
				}
			}
		});
	}
	else
	{
		if (NextChapter)
		{
			NextChapter->Start();
		}
		else
		{
			UGameplayStatics::OpenLevel(GetWorld(), TEXT("MainMenu"));
		}
	}

}

void ALAPlayerController::QuitToMainMenu()
{
	TransitionToChapter(nullptr);
}


void ALAPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (TransitionTimeRemaining > 0.f)
	{
		TransitionTimeRemaining -= DeltaSeconds;

		if (TransitionTimeRemaining <= 0.f)
		{
			TransitionTimeRemaining = 0.f;

			if (NextChapter)
			{
				NextChapter->Start();
			}
			else
			{
				UGameplayStatics::OpenLevel(GetWorld(), TEXT("MainMenu"));
			}
			return;
		}
	}

	if (PlayerCameraManager->GetCameraCacheTime() > 0.f && RadarEvents.Num() > 0)
	{
		FVector OutCamLoc;
		FRotator OutCamRot;
		PlayerCameraManager->GetCameraViewPoint(OutCamLoc, OutCamRot);

		for (int32 i = 0; i < RadarEvents.Num();)
		{
			ULARadar* Radar = RadarEvents[i];
			bool bFoundActorWithTag = false;
			for (TActorIterator<ATargetPoint> It(GetWorld()); It; ++It)
			{
				AActor* Actor = *It;
				if (!Actor->IsPendingKill())
				{
					if (Actor->ActorHasTag(Radar->RadarTag))
					{
						bFoundActorWithTag = true;
						FVector DirectionToActor = (Actor->GetActorLocation() - OutCamLoc).GetSafeNormal2D();
						float Delta = FMath::FindDeltaAngleDegrees(DirectionToActor.ToOrientationRotator().Yaw, OutCamRot.Yaw);
						UE_LOG(LALog, Warning, TEXT("Delta: %f"), Delta);
						float FOVToCheck = PlayerCameraManager->GetFOVAngle() * .9f *.5f;
						bool bActorVisible = FMath::Abs(Delta) < FOVToCheck;

						if ((bActorVisible && !Radar->bInvert) || (Radar->bInvert && !bActorVisible))
						{
							Radar->Trigger();
							break; // the event was already triggered, its not going to be triggered again so stop searching
						}

					}
				}
			}

			// if we never found the actor with tag and we didn't trigger, then it was probably deleted
			if (Radar->bInvert && !bFoundActorWithTag && !Radar->WasTriggered())
			{
				Radar->Trigger();
			}

			// if the event was triggered, remove it and dont move forward in the array
			if (Radar->WasTriggered())
			{
				RadarEvents.RemoveAt(i);
			}
			else // if it wasn't triggered, then move to the next element
			{
				++i;
			}
		}
	}
}

void ALAPlayerController::RegisterRadarEvent(ULARadar* RadarEvent)
{
	RadarEvents.AddUnique(RadarEvent);
}

void ALAPlayerController::RegisterCode(ULACode* NewCode)
{
	ActiveCodes.AddUnique(NewCode);
}
