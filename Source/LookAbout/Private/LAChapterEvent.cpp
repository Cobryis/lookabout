
#include "LAChapterEvent.h"
#include "LookAbout.h"

#include "LAEvent.h"
#include "LAAction.h"

#include "Matinee/MatineeActor.h"

#include "Sound/AmbientSound.h"
#include "LACustomEvent.h"

#include "LACharacter.h"

#include "LAHUD.h"

ELAChapterEventType GetChapterEventTypeFromString(const FString& String)
{
	if (String.Equals(TEXT("JUMP"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::JUMP;
	}
	else if (String.Equals(TEXT("SUBTITLE"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::SUBTITLE;
	}
	else if (String.Equals(TEXT("MATINEE"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::MATINEE;
	}
	else if (String.Equals(TEXT("TEXT"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::TEXT;
	}
	else if (String.Equals(TEXT("SOUND"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::SOUND;
	}
	else if (String.Equals(TEXT("END"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::ENDGAME;
	}
	else if (String.Equals(TEXT("HEALTH"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::HEALTH;
	}
	else if (String.Equals(TEXT("CURSOR"), ESearchCase::IgnoreCase))
	{
		return ELAChapterEventType::CURSOR;
	}
	else
	{
		return ELAChapterEventType::NONE;
	}
}

ELACommand GetCommandFromString(const FString& String)
{
	if (String.Equals(TEXT("SCENE"), ESearchCase::IgnoreCase))
	{
		return ELACommand::SCENE;
	}
	else if (String.Equals(TEXT("EVENT"), ESearchCase::IgnoreCase))
	{
		return ELACommand::EVENT;
	}
	else if (String.Equals(TEXT("OPTION"), ESearchCase::IgnoreCase))
	{
		return ELACommand::OPTION;
	}
	else if (String.Equals(TEXT("ACTION"), ESearchCase::IgnoreCase))
	{
		return ELACommand::ACTION;
	}
	else if (String.Equals(TEXT("CODE"), ESearchCase::IgnoreCase))
	{
		return ELACommand::CODE;
	}
	else if (String.Equals(TEXT("CLICK"), ESearchCase::IgnoreCase))
	{
		return ELACommand::CLICK;
	}
	else if (String.Equals(TEXT("RADAR"), ESearchCase::IgnoreCase))
	{
		return ELACommand::RADAR;
	}
	else if (String.Equals(TEXT("HEALTH"), ESearchCase::IgnoreCase))
	{
		return ELACommand::HEALTH;
	}
	else if (String.Equals(TEXT("HEALTHMULTI"), ESearchCase::IgnoreCase))
	{
		return ELACommand::HEALTHMULTI;
	}
	else
	{
		return ELACommand::NONE;
	}
}

ULAChapterEvent* ULAChapterEvent::ConstructFromTableRow(ULAChapter* Parent, const FLATableRow& TableRow, FName RowName)
{
	ULAChapterEvent* ExistingEvent = FindObject<ULAChapterEvent>(Parent, *RowName.ToString());

	check(!ExistingEvent);

	ELACommand Command = GetCommandFromString(TableRow.Command);
	ULAChapterEvent* ChapterEvent = nullptr;
	if (Command == ELACommand::ACTION || Command == ELACommand::OPTION)
	{
		ChapterEvent = NewObject<ULAAction>(Parent, RowName);
	}
	else if (Command == ELACommand::EVENT)
	{
		ChapterEvent = NewObject<ULAEvent>(Parent, RowName);
	}
	else if (Command == ELACommand::CODE)
	{
		ChapterEvent = NewObject<ULACode>(Parent, RowName);
	}
	else if (Command == ELACommand::CLICK)
	{
		ChapterEvent = NewObject<ULAClick>(Parent, RowName);
	}
	else if (Command == ELACommand::RADAR)
	{
		ChapterEvent = NewObject<ULARadar>(Parent, RowName);
	}
	else if (Command == ELACommand::HEALTH)
	{
		ChapterEvent = NewObject<ULAHealthEvent>(Parent, RowName);
	}
	else if (Command == ELACommand::HEALTHMULTI)
	{
		ChapterEvent = NewObject<ULAHealthEvent>(Parent, RowName);
		static_cast<ULAHealthEvent*>(ChapterEvent)->bMulti = true;
	}
	else
	{
		UE_LOG(LALog, Error, TEXT("Invalid Command given for ChapterEvent \"%s\""), *TableRow.Command);
	}

	if (ChapterEvent)
	{
		ChapterEvent->Command = Command;
		ChapterEvent->Init(TableRow);
	}

	return ChapterEvent;
}

ALAHUD* ULAChapterEvent::GetHUD() const
{
	if (APlayerController* PlayerController = GetOwningChapter()->GetGameInstance()->GetFirstLocalPlayerController())
	{
		return Cast<ALAHUD>(PlayerController->GetHUD());
	}

	return nullptr;
}

void ULAChapterEvent::BindJump( const FLATableRow& TableRow )
{
	ULAChapter* NextChapter = Cast<ULAGameInstance>(GetOuter()->GetOuter())->NewChapterFromCSV(TableRow.AssetName);

	if (NextChapter == nullptr)
	{
		UE_LOG(LALog, Warning, TEXT("Unable to create chapter with name \"%s\""), *TableRow.AssetName);
	}
	else
	{
		OnTrigger.BindLambda([this, NextChapter]()
		{
			if (GetOwningChapter() && GetOwningChapter()->GetGameInstance())
			{
				GetOwningChapter()->GetGameInstance()->TransitionToChapter(NextChapter);
			}
			else
			{
				NextChapter->Start();
			}
		});
	}
}

void ULAChapterEvent::BindText( const FLATableRow& TableRow )
{
	FString FullPath = FPaths::ConvertRelativePathToFull(FString::Printf(TEXT("%sText/%s.txt"), *FPaths::ProjectContentDir(), *TableRow.AssetName));
	FString Text;
	if (FFileHelper::LoadFileToString(Text, *FullPath))
	{
		OnTrigger.BindLambda([this, Text]()
		{
			if (ALAHUD* HUD = GetHUD())
			{
				HUD->ShowText(Text);
			}
		});
	}
	else
	{
		UE_LOG(LALog, Error, TEXT("Unable to load \"%s\" for \"%s\""), *TableRow.AssetName, *GetFullName());
	}
}

void ULAChapterEvent::BindSubtitle( const FLATableRow& TableRow )
{
	FString Subtitle(TableRow.AssetName);
	float SubtitleTime = FCString::Atof(*Detail);

	if (SubtitleTime <= 0)
	{
		SubtitleTime = Subtitle.Len() * .1f;

		UE_LOG(LALog, Error, TEXT("Subtitle time was not provided in Detail of \"%s\""), *GetFullName());
	}

	OnTrigger.BindLambda([this, Subtitle, SubtitleTime]
	{
		if (ALAHUD* HUD = GetHUD())
		{
			HUD->ShowSubtitle(Subtitle, SubtitleTime);
		}
	});
}

void ULAChapterEvent::BindMatinee( const FLATableRow& TableRow )
{
	FName MatineeName = *TableRow.AssetName;
	OnTrigger.BindLambda([this, MatineeName]
	{
		if (ULAChapter* Owner = GetOwningChapter())
		{
			if (ULAGameInstance* GameInstance = Owner->GetGameInstance())
			{
				if (UWorld* World = GameInstance->GetWorld())
				{
					for (TActorIterator<AMatineeActor> It(World, AMatineeActor::StaticClass()); It; ++It)
					{
						AMatineeActor* Matinee = *It;
						if (!Matinee->IsPendingKill() && Matinee->GetFName().IsEqual(MatineeName, ENameCase::IgnoreCase, false))
						{
							if (Matinee->bIsPlaying)
							{
								Matinee->Pause();
							}
							else
							{
								Matinee->Play();
							}
							break;
						}
					}
				}
			}
		}
	});
}

void ULAChapterEvent::BindSound(const FLATableRow& TableRow)
{
	FName SoundName = *TableRow.AssetName;
	OnTrigger.BindLambda([this, SoundName]
	{
		if (ULAChapter* Owner = GetOwningChapter())
		{
			if (ULAGameInstance* GameInstance = Owner->GetGameInstance())
			{
				if (UWorld* World = GameInstance->GetWorld())
				{
					for (TActorIterator<AAmbientSound> It(World, AAmbientSound::StaticClass()); It; ++It)
					{
						AAmbientSound* Sound = *It;
						if (!Sound->IsPendingKill() && Sound->GetFName().IsEqual(SoundName, ENameCase::IgnoreCase, false))
						{
							Sound->GetAudioComponent()->Play();
							break;
						}
					}
				}
			}
		}
	});
}

void ULAChapterEvent::BindHealth(const FLATableRow& TableRow)
{
	float HealthAmount = FCString::Atof(*TableRow.AssetName);
	OnTrigger.BindLambda([this, HealthAmount]
	{
		if (ALACharacter* Character = Cast<ALACharacter>(UGameplayStatics::GetPlayerPawn(GetOwningChapter()->GetGameInstance(), 0)))
		{
			Character->ModifyHealth(HealthAmount);
		}
	});
}

void ULAChapterEvent::BindEndGame(const FLATableRow& TableRow)
{
	OnTrigger.BindLambda([this]
	{
		if (ULAChapter* Owner = GetOwningChapter())
		{
			if (ULAGameInstance* GameInstance = Owner->GetGameInstance())
			{
				GameInstance->ResetSaveGame();
				GameInstance->QuitToMainMenu();
			}
		}
	});
}

void ULAChapterEvent::BindCursor(const FLATableRow& TableRow)
{
	static const TArray<FString> MouseCursorStrings =
	{
		TEXT("None"),
		TEXT("Default"),
		TEXT("TextEditBeam"),
		TEXT("ResizeLeftRight"),
		TEXT("ResizeUpDown"),
		TEXT("ResizeSouthEast"),
		TEXT("ResizeSouthWest"),
		TEXT("CardinalCross"),
		TEXT("Crosshairs"),
		TEXT("Hand"),
		TEXT("GrabHand"),
		TEXT("GrabHandClosed"),
		TEXT("SlashedCircle"),
		TEXT("EyeDropper"),
		TEXT("Custom")
	};

	EMouseCursor::Type MouseCursor = EMouseCursor::None;
	int32 FoundCursor;
	if (MouseCursorStrings.Find(TableRow.Detail, FoundCursor))
	{
		if (FoundCursor >= 0 && FoundCursor < EMouseCursor::TotalCursorCount)
		{
			MouseCursor = EMouseCursor::Type(FoundCursor);
		}
	}

	FName CursorAsset = *TableRow.AssetName;
	OnTrigger.BindLambda([this, MouseCursor, CursorAsset]
	{ 
		if (ULAChapter* Owner = GetOwningChapter())
		{
			if (ULAGameInstance* GameInstance = Owner->GetGameInstance())
			{
				GameInstance->SetGameHardwareCursor(MouseCursor, CursorAsset, FVector2D(EForceInit::ForceInitToZero));
			}
		}
	});
}

void ULAChapterEvent::Init(const FLATableRow& TableRow)
{
	check(GetOwningChapter());
	TimeOrAngle = TableRow.TimeOrAngle;

	Type = GetChapterEventTypeFromString(TableRow.Type);

	if (Type == ELAChapterEventType::NONE)
	{
		FString ClassName = TEXT("/Game/CustomEvents/") + TableRow.Type + TEXT(".") + TableRow.Type + TEXT("_C");
		UClass* CustomClass = LoadObject<UClass>(nullptr, *ClassName);
		if (CustomClass == nullptr)
		{
			UE_LOG(LALog, Error, TEXT("Invalid Type \"%s\" given for Chapter \"%s\""), *TableRow.Type, *GetFullName());
		}
		else
		{
			CustomEventClass = CustomClass;
			Type = ELAChapterEventType::CUSTOM;
			AssetName = TableRow.AssetName;
		}
	}

	Detail = TableRow.Detail;

	switch (Type)
	{
	case ELAChapterEventType::JUMP:
		BindJump(TableRow);
		break;
	case ELAChapterEventType::TEXT:
		BindText(TableRow);
		break;
	case ELAChapterEventType::SUBTITLE:
		BindSubtitle(TableRow);
		break;
	case ELAChapterEventType::MATINEE:
		BindMatinee(TableRow);
		break;
	case ELAChapterEventType::SOUND:
		BindSound(TableRow);
		break;
	case ELAChapterEventType::ENDGAME:
		BindEndGame(TableRow);
		break;
	case ELAChapterEventType::HEALTH:
		BindHealth(TableRow);
		break;
	case ELAChapterEventType::CURSOR:
		BindCursor(TableRow);
		break;
	}
}

void ULAChapterEvent::Trigger()
{
	if (OnTrigger.IsBound())
	{
		OnTrigger.Execute();
		bTriggered = true;
	}
}

float ULAChapterEvent::GetActivateTime() const
{
	return TimeOrAngle;
}

void ULAChapterEvent::AutoActivate(float FastForward)
{
	Trigger();
}

void ULAChapterEvent::SetupTimer(FTimerManager& TimerManager)
{
	float ActivateTime = GetActivateTime();

	if (ActivateTime <= 0.0f)
	{
		UE_LOG(LALog, Warning, TEXT("Activate time for ChapterEvent \"%s\" is <= 0.0"), *GetFullName());

		AutoActivate(ActivateTime * -1.f);
	}
	else
	{
		FTimerDelegate Delegate;
		Delegate.BindLambda([this]()
		{
			AutoActivate();
		});

		TimerManager.SetTimer(TimerHandle, Delegate, ActivateTime, false);
	}
}

void ULAChapterEvent::ClearTimer( FTimerManager& TimerManager )
{
	TimerManager.ClearTimer(TimerHandle);
}

void ULAChapterEvent::InitCustomEvent()
{
	FTransform Transform;
	CustomEvent = GetOuter()->GetOuter()->GetWorld()->SpawnActorDeferred<ALACustomEvent>(CustomEventClass, Transform);
	CustomEvent->BP_InitEvent(AssetName, Detail);
	UGameplayStatics::FinishSpawningActor(CustomEvent, Transform);
	OnTrigger.BindLambda([this]
	{
		CustomEvent->BP_TriggerEvent();
	});
}

void ULAChapterEvent::ChapterInit(ELAGameMode GameMode)
{
	if (Type == ELAChapterEventType::CUSTOM)
	{
		InitCustomEvent();
	}

	bTriggered = false;
}
