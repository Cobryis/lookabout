
#include "LAGameInstance.h"
#include "LookAbout.h"

#include "LAChapter.h"
#include "LAHUD.h"
#include "LAGameMode.h"
#include "LAPlayerController.h"

#include "UObject/Class.h"
#include "GameMapsSettings.h"
#include "CsvParser.h"
#include "Engine/DataTable.h"
#include "DataTableUtils.h"

FString ULASaveGame::SaveSlotName = TEXT("Data");


void SLALoadingScreen::Construct(const FArguments& InArgs, UTexture2D* Image)
{
	BlackBackground = MakeShareable(new FSlateColorBrush(FColor::Black));
	FVector2D ImageSize(EForceInit::ForceInitToZero);
	if (Image != nullptr)
	{
		ImageSize.X = Image->GetSizeX();
		ImageSize.Y = Image->GetSizeY();
	}
	ImageBrush = MakeShareable(new FLALoadingScreenBrush(Image, ImageSize));

	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			[
				SNew(SImage)
				.Image(BlackBackground.Get())
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SImage)
				.Image(ImageBrush.Get())
			]
		];
	
}

ULAGameInstance::ULAGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	LastCursorShape = EMouseCursor::Default;
	LastCursorContentPath = NAME_None;
	LastCursorHotSpot = FVector2D(EForceInit::ForceInitToZero);
}

UDataTable* ULAGameInstance::LoadCSV(const FString& Filename)
{
	UDataTable* NewTable = nullptr;
	FString Data;
	FString FullPath = FPaths::ConvertRelativePathToFull(FString::Printf(TEXT("%sChapters/%s.csv"), *FPaths::ProjectContentDir(), *Filename));
	if (FFileHelper::LoadFileToString(Data, *FullPath))
	{

		NewTable = NewObject<UDataTable>(GetTransientPackage(), *(Filename + TEXT("_DT")));

		if (NewTable)
		{
			TArray<FString> OutProblems;

			UScriptStruct* RowStruct = FindObjectChecked<UScriptStruct>(ANY_PACKAGE, FLATableRow::GetTCHARName());

			if (Data.IsEmpty())
			{
				OutProblems.Add(FString(TEXT("Input data is empty.")));
			}
			else
			{
				const FCsvParser Parser(Data);
				const auto& Rows = Parser.GetRows();

				// Must have at least 2 rows (column names + data)
				if (Rows.Num() <= 1)
				{
					OutProblems.Add(FString(TEXT("Too few rows.")));
				}
				else
				{
					// Find property for each column
					TArray<UProperty*> ColumnProps; // = NewTable->GetTablePropertyArray(Rows[0], RowStruct, OutProblems);

					// Get list of all expected properties from the struct
					TArray<FName> ExpectedPropNames = DataTableUtils::GetStructPropertyNames(RowStruct);

					const TArray<const TCHAR*>& Cells = Rows[0];

					// Need at least 2 columns, first column is skipped, will contain row names
					if (Cells.Num() > 1)
					{
						ColumnProps.AddZeroed(Cells.Num());

						// first element always NULL - as first column is row names

						for (int32 ColIdx = 1; ColIdx < Cells.Num(); ++ColIdx)
						{
							const TCHAR* ColumnValue = Cells[ColIdx];

							FName PropName = DataTableUtils::MakeValidName(ColumnValue);
							if (PropName == NAME_None)
							{
								OutProblems.Add(FString(TEXT("Missing name for column %d."), ColIdx));
							}
							else
							{
								UProperty* ColumnProp = FindField<UProperty>(RowStruct, PropName);

								for (TFieldIterator<UProperty> It(RowStruct); It && !ColumnProp; ++It)
								{
									static const FName DisplayNameKey(TEXT("DisplayName"));
// 									const auto Prop = *It;
// 									const auto DisplayName(Prop && Prop->HasMetaData(DisplayNameKey)) ? Prop->GetMetaData(DisplayNameKey) : DefaultName;
									ColumnProp = *It; // (!DisplayName.IsEmpty() && DisplayName == ColumnValue) ? *It : NULL;
								}

								// Didn't find a property with this name, problem..
								if (ColumnProp == NULL)
								{
									OutProblems.Add(FString::Printf(TEXT("Cannot find Property for column '%s' in struct '%s'."), *PropName.ToString(), *RowStruct->GetName()));
								}
								// Found one!
								else
								{
									// Check we don't have this property already
									if (ColumnProps.Contains(ColumnProp))
									{
										OutProblems.Add(FString::Printf(TEXT("Duplicate column '%s'."), *ColumnProp->GetName()));
									}
									// Check we support this property type
									else if (!DataTableUtils::IsSupportedTableProperty(ColumnProp))
									{
										OutProblems.Add(FString::Printf(TEXT("Unsupported Property type for struct member '%s'."), *ColumnProp->GetName()));
									}
									// Looks good, add to array
									else
									{
										ColumnProps[ColIdx] = ColumnProp;
									}

									// Track that we found this one
									ExpectedPropNames.Remove(ColumnProp->GetFName());
								}
							}
						}
					}

					// Iterate over rows
					for (int32 RowIdx = 1; RowIdx < Rows.Num(); RowIdx++)
					{
						const TArray<const TCHAR*>& RowCells = Rows[RowIdx];

						// Need at least 1 cells (row name)
						if (RowCells.Num() < 1)
						{
							OutProblems.Add(FString::Printf(TEXT("Row '%d' has too few cells."), RowIdx));
							continue;
						}

						// Need enough columns in the properties!
						if (ColumnProps.Num() < RowCells.Num())
						{
							OutProblems.Add(FString::Printf(TEXT("Row '%d' has more cells than properties, is there a malformed string?"), RowIdx));
							continue;
						}

						// Get row name
						FName RowName = DataTableUtils::MakeValidName(RowCells[0]);

						// Check its not 'none'
						if (RowName == NAME_None)
						{
							OutProblems.Add(FString::Printf(TEXT("Row '%d' missing a name."), RowIdx));
							continue;
						}

						TMap<FName, uint8*>& RowMap = const_cast<TMap<FName, uint8*>&>(NewTable->GetRowMap());
						
						if (RowMap.Find(RowName) != nullptr)
						{
							FString BaseName = RowName.ToString();
							FString NewName;
							int NameIt;
							for (NameIt = 1; NameIt < 1000; ++NameIt) // if we hit 1000, we have other problems
							{
								FString TestName = BaseName + FString::Printf(TEXT("_%d"), NameIt);
								if (RowMap.Find(*TestName) == nullptr)
								{
									NewName = TestName;
									break;
								}
							}

							if (NewName.IsEmpty())
							{
								UE_LOG(LALog, Error, TEXT("Too many commands with same name in '%s', command was not created."), *Filename);
								return nullptr;
							}
							else
							{
								RowName = *NewName;
							}
						}

						// Check its not a duplicate
// 						if (RowMap.Find(RowName) != NULL)
// 						{
// 							OutProblems.Add(FString::Printf(TEXT("Duplicate row name '%s'."), *RowName.ToString()));
// 							continue;
// 						}

						// Allocate data to store information, using UScriptStruct to know its size
						uint8* RowData = (uint8*)FMemory::Malloc(RowStruct->PropertiesSize);
						RowStruct->InitializeStruct(RowData);
						// And be sure to call DestroyScriptStruct later

						// Add to row map
						RowMap.Add(RowName, RowData);

						// Now iterate over cells (skipping first cell, that was row name)
						for (int32 CellIdx = 1; CellIdx < RowCells.Num(); CellIdx++)
						{
							// Try and assign string to data using the column property
							UProperty* ColumnProp = ColumnProps[CellIdx];
							const FString CellValue = RowCells[CellIdx];
							FString Error = DataTableUtils::AssignStringToProperty(CellValue, ColumnProp, RowData);

							// If we failed, output a problem string
							if (Error.Len() > 0)
							{
								FString ColumnName = ColumnProp->GetName();
// 								FString ColumnName = (ColumnProp != NULL)
// 									? FPropertyDisplayNameHelper::Get(ColumnProp, ColumnProp->GetName())
// 									: FString(TEXT("NONE"));
								OutProblems.Add(FString::Printf(TEXT("Problem assigning string '%s' to property '%s' on row '%s' : %s"), *CellValue, *ColumnName, *RowName.ToString(), *Error));
							}
						}

						// Problem if we didn't have enough cells on this row
						if (RowCells.Num() < ColumnProps.Num())
						{
							OutProblems.Add(FString::Printf(TEXT("Too few cells on row '%s'."), *RowName.ToString()));
						}
					}

					NewTable->Modify(true);
				}

			}

			// Print out
			// UE_LOG(LogCSVImportFactory, Log, TEXT("Imported DataTable '%s' - %d Problems"), *InName.ToString(), Problems.Num());

			if (OutProblems.Num() > 0)
			{
				FString AllProblems;

				for (int32 ProbIdx = 0; ProbIdx < OutProblems.Num(); ProbIdx++)
				{
					// Output problems to log
					// UE_LOG(LogCSVImportFactory, Log, TEXT("%d:%s"), ProbIdx, *Problems[ProbIdx]);
					AllProblems += OutProblems[ProbIdx];
					AllProblems += TEXT("\n");
				}
			}
		}
	}

	return NewTable;
}

void ULAGameInstance::SaveGame()
{
	if (CurrentChapter && CurrentChapter->bCanBeLoadedInto)
	{
		ULASaveGame* SaveGame = Cast<ULASaveGame>(UGameplayStatics::CreateSaveGameObject(ULASaveGame::StaticClass()));

		SaveGame->CurrentChapterName = CurrentChapter->ChapterName;

		UGameplayStatics::SaveGameToSlot(SaveGame, ULASaveGame::SaveSlotName, 0);
	}
}

void ULAGameInstance::LoadAndStartGame()
{
	ULASaveGame* SaveGame = Cast<ULASaveGame>(UGameplayStatics::LoadGameFromSlot(ULASaveGame::SaveSlotName, 0));

	CurrentChapter = StartChapter;

	if (SaveGame != nullptr && !SaveGame->CurrentChapterName.IsEmpty())
	{
		bool bFoundChapter = false;

		for (ULAChapter* Chapter : Chapters)
		{
			if (Chapter->ChapterName.Equals(SaveGame->CurrentChapterName))
			{
				CurrentChapter = Chapter;
				bFoundChapter = true;
				break;
			}
		}

		if (!bFoundChapter)
		{
			UE_LOG(LALog, Error, TEXT("Failed to find chapter '%s' in save file. Resetting save game."), *SaveGame->CurrentChapterName);
			ResetSaveGame();
		}
	}

	TransitionToChapter(CurrentChapter);
}

void ULAGameInstance::RestartLevel()
{
	TransitionToChapter(LastLoadableChapter);
}

void ULAGameInstance::ResetSaveGame()
{
	UGameplayStatics::DeleteGameInSlot(ULASaveGame::SaveSlotName, 0);
}

bool ULAGameInstance::DoesSaveGameExist()
{
	return UGameplayStatics::DoesSaveGameExist(ULASaveGame::SaveSlotName, 0);
}

void ULAGameInstance::StartGameInstance()
{
	StartChapter = NewChapterFromCSV(TEXT("Start"));

	UTexture2D* LoadingImage = nullptr;
	if (GetDefault<ALAGameMode>()->HUDClass)
	{
		LoadingImage = GetDefault<ALAHUD>(GetDefault<ALAGameMode>()->HUDClass)->LoadTexture;
	}

	LoadingScreen.WidgetLoadingScreen = SNew(SLALoadingScreen, LoadingImage); 

	Super::StartGameInstance();
}

bool ULAGameInstance::StartChapterWithName(const FString& ChapterName)
{
	for (ULAChapter* Chapter : Chapters)
	{
		if (Chapter->ChapterName.Equals(ChapterName))
		{
			TransitionToChapter(Chapter);
			return true;
		}
	}

	ULAChapter* Chapter = NewChapterFromCSV(ChapterName);
	if (Chapter != nullptr)
	{
		TransitionToChapter(Chapter);
		return true;
	}

	UE_LOG(LALog, Warning, TEXT("Chapter not found with name: %s"), *ChapterName);
	return false;
}

#if WITH_EDITOR

FGameInstancePIEResult ULAGameInstance::StartPlayInEditorGameInstance(ULocalPlayer* LocalPlayer, const FGameInstancePIEParameters& Params)
{
	FGameInstancePIEResult Result = Super::StartPlayInEditorGameInstance(LocalPlayer, Params);

	if (Result.bSuccess)
	{
		StartChapter = NewChapterFromCSV("Start");
		if (!Params.bSimulateInEditor && GetWorld() && GetWorld()->GetCurrentLevel())
		{
			CurrentChapter = StartChapter;

			FString CurrentLevelURL = GetWorld()->GetMapName();

			for (ULAChapter* LoadedChapter : Chapters)
			{
				FString Path;
				FString Filename;
				FString Extension;
				FPaths::Split(LoadedChapter->SceneName, Path, Filename, Extension);
				if (PIEMapName.Contains(Filename))
				{
					CurrentChapter = LoadedChapter;
					CurrentChapter->Start();
					break;
				}
			}
		}
	}

	return Result;
}

#endif // WITH_EDITOR

void ULAGameInstance::ResetToLastGameHardwareCursor()
{
	SetGameHardwareCursor(LastCursorShape, LastCursorContentPath, LastCursorHotSpot);
}

void ULAGameInstance::SetGameHardwareCursor(EMouseCursor::Type CursorShape, FName CursorContentPath, FVector2D HotSpot)
{
	if (GetWorld() && GetWorld()->GetGameViewport())
	{
		GetWorld()->GetGameViewport()->SetHardwareCursor(CursorShape, CursorContentPath, HotSpot);
	}
}

void ULAGameInstance::TransitionToChapter(ULAChapter* NextChapter)
{
	if (ALAPlayerController* PC = Cast<ALAPlayerController>(GetFirstLocalPlayerController(GetWorld())))
	{
		PC->TransitionToChapter(NextChapter);
	}
}

ULAChapter* ULAGameInstance::NewChapterFromCSV(FString ChapterName)
{
	if (ChapterName.IsEmpty())
	{
		return nullptr;
	}

	ULAChapter* Chapter = FindObject<ULAChapter>(this, *ChapterName);
	if (Chapter == nullptr)
	{
		const UDataTable* const ChapterTable = LoadCSV(ChapterName);
		if (ChapterTable != nullptr && ChapterTable->GetRowMap().Num() > 0)
		{
			Chapter = ULAChapter::ConstructFromTable(this, ChapterTable, ChapterName);

			Chapters.Add(Chapter);
		}
		else
		{
			UE_LOG(LALog, Error, TEXT("Failed to find chapter: %s"), *ChapterName);
		}
	}

	return Chapter;
}

class ALAHUD* ULAGameInstance::GetHUD() const
{
	APlayerController* PC = GetFirstLocalPlayerController();
	return PC ? Cast<ALAHUD>(PC->GetHUD()) : nullptr;
}

void ULAGameInstance::ShowLoadingScreen() const
{

	GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
}

ELAGameMode ULAGameInstance::GetCurrentGameMode() const
{
	return CurrentChapter->GameMode;
}

void ULAGameInstance::QuitToMainMenu()
{
	if (ALAPlayerController* PC = Cast<ALAPlayerController>(GetFirstLocalPlayerController(GetWorld())))
	{
		PC->QuitToMainMenu();
	}
}

void ULAGameInstance::ChangeSoundclassVolume(USoundClass* SoundClass, float NewVolume)
{
	if (SoundClass)
	{
		SoundClass->Properties.Volume = NewVolume;
	}
	else
	{
		UE_LOG(LALog, Warning, TEXT("Attempting to change volume on null SoundClass"));
	}
}
