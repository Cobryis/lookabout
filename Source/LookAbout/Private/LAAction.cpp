
#include "LAAction.h"
#include "LookAbout.h"

#include "LAChapter.h"

#include "LAHUD.h"

void ULAAction::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);

	DisplayText = TableRow.Detail;
}

void ULAAction::ChapterInit(ELAGameMode GameMode)
{
	Super::ChapterInit(GameMode);

	if (GameMode == ELAGameMode::EXPLORE)
	{
		if (ALAHUD* HUD = GetHUD())
		{
			HUD->AddNewAction(this, 0.0f);
		}
	}
	else
	{
		SetupTimer(GetOwningChapter()->GetGameInstance()->GetWorld()->GetTimerManager());
	}
}

float ULAAction::GetActivateTime() const
{
	ULAChapter* Owner = GetOwningChapter();
	if (Owner)
	{
		return Super::GetActivateTime() - Owner->NodeTime;
	}

	return Super::GetActivateTime();
}

void ULAAction::AutoActivate(float FastForward)
{
	if (GetOwningChapter()->GameMode == ELAGameMode::RHYTHM)
	{
		if (ALAHUD* HUD = GetHUD())
		{
			HUD->AddNewAction(this, FastForward);
			return;
		}
		else
		{
			UE_LOG(LALog, Error, TEXT("Failed to auto activate Action \"%s\": invalid HUD"), *GetFullName());
		}
	}
	
	Super::AutoActivate();
}

void ULAClick::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);

	if (uint32(TimeOrAngle) < 1)
	{
		UE_LOG(LALog, Error, TEXT("TimeOrAngle < 1 for Click \"%s\""), *GetFullName());
	}

	if (Detail.IsEmpty())
	{
		UE_LOG(LALog, Error, TEXT("Detail Empty for Click \"%s\""), *GetFullName());
	}
}

void ULAClick::ChapterInit(ELAGameMode GameMode)
{
	Super::ChapterInit(GameMode);

	NumClicks = 0;

	Tag = FName(*Detail, FNAME_Find);

	if (Tag == NAME_None)
	{
		UE_LOG(LALog, Warning, TEXT("Actors with Tag \"%s\" not found for Click \"%s\""), *Detail, *GetFullName());
	}
	else
	{
		for (FActorIterator It(GetOwningChapter()->GetGameInstance()->GetWorld()); It; ++It)
		{
			if (It->ActorHasTag(Tag))
			{
				It->OnClicked.AddUniqueDynamic(this, &ULAClick::OnActorClicked);
			}
		}
	}
}

void ULAClick::OnActorClicked(AActor* TouchedActor, FKey ButtonPressed)
{
	if (ButtonPressed == EKeys::LeftMouseButton)
	{
		NumClicks++;
		if (NumClicks >= uint32(TimeOrAngle))
		{
			Trigger();
		}
		
	}
}
