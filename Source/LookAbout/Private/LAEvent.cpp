
#include "LAEvent.h"
#include "LookAbout.h"

#include "LAChapter.h"
#include "LACharacter.h"
#include "LAPlayerController.h"
#include "LAHUD.h"

void ULAEvent::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);

}

void ULAEvent::ChapterInit(ELAGameMode GameMode)
{
	Super::ChapterInit(GameMode);

	SetupTimer(GetOwningChapter()->GetGameInstance()->GetWorld()->GetTimerManager());
}

void ULACode::AutoActivate(float FastForward)
{
	// don't trigger until code is entered

	if (ALAPlayerController* PC = Cast<ALAPlayerController>(GetOwningChapter()->GetGameInstance()->GetFirstLocalPlayerController()))
	{
		PC->RegisterCode(this);
	}
}

void ULACode::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);

	if (Type == ELAChapterEventType::SUBTITLE)
	{
		UE_LOG(LALog, Warning, TEXT("SUBTITLE not supported for CODE in %s. Will be treated as normal EVENT."), *GetOwningChapter()->ChapterName);
	}
}

void ULACode::Update(FKey Key)
{
	if (!Detail.IsEmpty())
	{
		if (Detail.Mid(CurrentIndex, 1).StartsWith(Key.ToString(), ESearchCase::IgnoreCase))
		{
			CurrentIndex++;
			if (CurrentIndex == Detail.Len())
			{
				CurrentIndex = 0;
				GetOwningChapter()->GetGameInstance()->GetHUD()->HideText();
				Trigger();
			}
		}
	}
}

void ULARadar::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);

	RadarTag = *Detail;

	bInvert = TimeOrAngle < 0.f;
}

void ULARadar::ChapterInit(ELAGameMode GameMode)
{
	Super::ChapterInit(GameMode);
}

void ULARadar::AutoActivate(float FastForward /* = 0.0f */)
{
	if (ALAPlayerController* PC = Cast<ALAPlayerController>(GetOwningChapter()->GetGameInstance()->GetFirstLocalPlayerController()))
	{
		PC->RegisterRadarEvent(this);
	}
}

void ULAHealthEvent::Init(const FLATableRow& TableRow)
{
	Super::Init(TableRow);
}

void ULAHealthEvent::ChapterInit(ELAGameMode GameMode)
{
	Super::ChapterInit(GameMode);

	GetOwningChapter()->RegisterHealthEvent(this);

	if (TimeOrAngle <= 0)
	{
		bCanTrigger = true;
	}
	else // assuming we start at full health, we'll always be above the triggered threshold
	{
		bCanTrigger = false; // only really used for health going up right now
	}
}

void ULAHealthEvent::OnCharacterHealthUpdated(class ALACharacter* CHaracter)
{
	if (TimeOrAngle <= 0)
	{
		if (CHaracter->Health <= FMath::Abs(TimeOrAngle))
		{
			if (bCanTrigger && !bTriggered)
			{
				Trigger();
			}
		}
		else
		{
			if (bMulti)
			{
				bTriggered = false;
			}
		}
	}
	else
	{
		if (CHaracter->Health >= TimeOrAngle)
		{
			if (bCanTrigger && !bTriggered)
			{
				Trigger();
			}
		}
		else
		{
			bCanTrigger = true;

			if (bMulti)
			{
				bTriggered = false;
			}
		}
	}
}
