using UnrealBuildTool;

public class LookAbout : ModuleRules
{
	public LookAbout(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateDependencyModuleNames.AddRange(new string[] { "Core", "Engine", "InputCore", "UMG", "SlateCore", "Slate", "MoviePlayer", "EngineSettings", "CoreUObject", "PBCharacterMovement" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });
	}
}
