using UnrealBuildTool;

public class LookAboutGame : ModuleRules
{
	public LookAboutGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "LookAbout" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });
	}
}
